import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JdctestconditionComponent } from './jdctestcondition.component';

describe('JdctestconditionComponent', () => {
  let component: JdctestconditionComponent;
  let fixture: ComponentFixture<JdctestconditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JdctestconditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JdctestconditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
