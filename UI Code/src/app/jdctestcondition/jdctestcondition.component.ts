import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { GlobalService } from '../global.service';
	
import * as EditorLib from 'datatables.net-editor-server';

@Component({
  selector: 'app-jdctestcondition',
  templateUrl: './jdctestcondition.component.html',
  styleUrls: ['./jdctestcondition.component.less']
})
export class JdctestconditionComponent implements OnInit {

  enableEdit = false;
  enableEditIndex = null;
  data:any;
  Deletedata:any;
  data2:any=[];
  URL:any;
  SGID:any;
  popup=false ;
  column:any;
   e = require('datatables.net-editor');
constructor(private http: HttpClient,private _global: GlobalService,private route: Router){
  this.URL=_global.URL;
//get request from web api
this.http.get(this.URL+'api/GetJdctestcondition/').subscribe(data => {

  this.data = data;
setTimeout(()=>{   
  $('#datatableexample').DataTable( {
    pagingType: 'full_numbers',
    pageLength: 10,
    processing: true,
    lengthMenu : [5, 10, 25],
    scrollX: true
} );
var element = document.getElementById("removeId");
element?.classList.remove("rowedit");
$('.DrillDownRow td').click(function(e){
  if($(this)[0].id!="removeId"){
    if (e.currentTarget.childNodes[0].nodeType != 1) {
  var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
  .find('input')
  .trigger('focus');
  console.log(td)
}
}
  });
  $('.rowedit').focusout(function(){
    var value:any=$(this).find('input').val();
    $(this).html(value);
              });
// $('#datatableexample').on( 'click', 'tbody td:not(:first-child)', function (e) {
//   if (e.currentTarget.childNodes[0].nodeType != 1) {
//   e.currentTarget.innerHTML='<input type="text" value="' + e.currentTarget.innerHTML + '"/>';
//   }
// } );
}, 1);
      }, error => console.error(error));
      this.http.get(this.URL+'api/GetTestConditionColumn/').subscribe(data => {
  
     //   this.column= data;
     
            }, error => console.error(error));
      this.http.get(this.URL+'api/GetTestCondition/').subscribe(data => {
this.data2="";
  this.data2 = data;
  this.data2 = JSON.parse(this.data2);
   console.log(this.data2);
   this.Deletedata=this.data2;
   this.data2.forEach((elm: { id: any; })=>delete elm.id)
  console.log(this.data2);
  this.column= this.data2;
 // console.log(JSON.parse(this.data2));
setTimeout(()=>{   
 $('#datatableexample1').DataTable( {
    pagingType: 'full_numbers',
    pageLength: 10,
    processing: true,
    lengthMenu : [5, 10, 25],
    scrollX: true,
    columnDefs: [{
      "defaultContent": "-",
      "targets": "_all"
    }]
} );
$('#datatableexample1').on( 'click', 'td', function (e) {
  if($(this)[0].id!="removeId1"){
    if (e.currentTarget.firstChild.childNodes[0].nodeType) {
  var td = $(this).html('<input type="text" value="' + e.currentTarget.firstChild.childNodes[0].textContent + '"/>')
  .find('input')
  .trigger('focus');
  console.log(td)
}
}

$('.rowedit1').focusout(function(e){
  var value:any=$(this).find('input').val();
  $(this).html(value);
            });
  });
var element = document.getElementById("removeId1");
element?.classList.remove("rowedit1");
$('.DrillDownRow1 td').click(function(e){
  if($(this)[0].id!="removeId1"){
    if (e.currentTarget.childNodes[0].nodeType != 1) {
  var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
  .find('input')
  .trigger('focus');
  console.log(td)
}
}
  });

  $('.rowedit1').focusout(function(){
    var value:any=$(this).find('input').val();
    $(this).html(value);
              });
}, 1);
      }, error => console.error(error));

}   
Savecolumn()
{

  var columnName=$("#ColumnName").val();

  this.http.get(this.URL+'api/AddColumn/'+columnName).subscribe(data => {
    if(data=="1")
   {
     alert("Column Added successfully");
     
     window.location.reload();
   }
});
}
   ngOnInit(){

  $('.popupCloseButton').click(function(){
      $('.hover_bkgr_fricc').hide();
  });

  this.http.get(this.URL+'api/Getcolum/xtest').subscribe(data => {

     this.column=data;
   
});
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
    else{
      $('.dateadded').on( 'change', function (ret :any) {
 
         var v = ret.target.value  // getting search input value
         
         $('#dataTables-example').DataTable().columns(3).search(v).draw();
     } );

    }
  }

  enableEditMethod(e: any, i: any) {
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
  }

  AddColumn()
  {
    
    $('.hover_bkgr_fricc').show();
  
  }

  AddRow()
  {
    if($('#QUV_name').val()=='')
    {
      alert("Please enter value in box");
    return;
    }
    else{
      $.fn.dataTable.ext.errMode = 'none';
    debugger;
    var t = $('#datatableexample').DataTable();
  
 
    
        t.row.add( [
          $('#QUV_name').val(),
           '',
            '',
            '',
            '',
            '',
            '',
            '<button id="remove" (click)="deleteRow(group)"><i class="fa fa-trash-o" style="font-size:24px"></i></button>'
        ] ).draw();
        $('#datatableexample tr').addClass('DrillDownRow1');
        $('#datatableexample td').addClass('rowedit');
        $('.DrillDownRow1 td').click(function(e){
    if(e.currentTarget.childNodes.length==0)
    {
      var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
      .find('input')
      .trigger('focus');
      console.log(td)
    }
            if (e.currentTarget.childNodes[0].nodeType != 1) {
          var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
          .find('input')
          .trigger('focus');
          console.log(td)
        
        }
          });
          $('.rowedit').focusout(function(){
            var value:any=$(this).find('input').val();
            $(this).html(value);
                      });
                    }
  }
  deleteRow(d:any){
    var result = confirm("Are sure you want to delete?");
if (result) {
    const index = this.data.indexOf(d);
  
    this.http.get(this.URL+'api/DeleteCondition/'+d.ID).subscribe(data => {
            if(data=="1")
           {
             alert("Row deleted successfully");
             this.data.splice(index, 1);
             window.location.reload();
           }
  });
}
}
  
  SaveUpdates()
  {
    var tableInfo = Array.prototype.map.call(document.querySelectorAll('#datatableexample tr'), function(tr){
      return Array.prototype.map.call(tr.querySelectorAll('td'), function(td){
        return td.innerHTML;
        });
      });
    
        let headers = new HttpHeaders();  
       
          headers.append('Content-Type', 'multipart/form-data');  
          headers.append('Accept', 'application/json');
         this.http.post<any>(this.URL+'api/InsertTestCondition',tableInfo , { headers }).subscribe(data => {
          console.log(data);
          alert("Data Saved Sucessfully")
          // window.location.reload();
    } ,error => {
      console.log(error);
      alert(error);
    },);
  }
  
  AddxenonRow()
  {
    if($('#xenon_name').val()=='')
    {
      alert("Please enter value in box");
    return;
    }
    else{
      $.fn.dataTable.ext.errMode = 'none';
    debugger;
    var t = $('#datatableexample1').DataTable();
  
 
    
        t.row.add( [
          '',
           '',
            '',
            $('#QUV_name1').val(),
            '',
            '',
            '',
            '<button id="remove" (click)="deletexenonRow(group)"><i class="fa fa-trash-o" style="font-size:24px"></i></button>'
        ] ).draw();
        t.order([1, 'asc']).draw();
      //  t.page('last').draw(false);
        $('#datatableexample1 tr').addClass('DrillDownRow1');
        $('#datatableexample1 td').addClass('rowedit1');
        $('.DrillDownRow1 td').click(function(e){
          if(e.currentTarget.childNodes.length==0)
    {
      var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
      .find('input')
      .trigger('focus');
      console.log(td)
    }
            if (e.currentTarget.childNodes[0].nodeType != 1) {
          var td = $(this).html('<input type="text" value="' + $(this).html() + '"/>')
          .find('input')
          .trigger('focus');
          console.log(td)
        
        }
          });
          $('.rowedit1').focusout(function(){
            var value:any=$(this).find('input').val();
            $(this).html(value);
                      });
                    }
  }
  deletexenonRow(d:any){
    var result = confirm("Are sure you want to delete?");
if (result) {
    const index = this.Deletedata.indexOf(d);
  console.log(d.Param);
    this.http.get(this.URL+'api/DeleteXenonCondition/'+d.Param).subscribe(data => {
            if(data=="1")
           {
             alert("Row deleted successfully");
             this.data.splice(index, 1);
             window.location.reload();
           }
  });
}
}
  
  SavexenonUpdates()
  {
    var tableInfo = Array.prototype.map.call(document.querySelectorAll('#datatableexample1 tr'), function(tr){
      return Array.prototype.map.call(tr.querySelectorAll('td'), function(td){
        return td.innerText;
        });
      });
    
        let headers = new HttpHeaders();  
       
          headers.append('Content-Type', 'multipart/form-data');  
          headers.append('Accept', 'application/json');
         this.http.post<any>(this.URL+'api/InsertXenonTestCondition',tableInfo , { headers }).subscribe(data => {
          console.log(data);
           alert("Data Saved Sucessfully")
           window.location.reload();
    },
    error => {
    console.log(error);
    });
}
}

