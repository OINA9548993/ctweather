

export class MICExcel {
     Formulation !:string;
         Pigment !:string;
       ID !:Float32Array;
        Date !:Date;
         duplicate !:Float32Array;
         Hours !:Float32Array;
         Lstar !:Float32Array;
         astar !:Float32Array;
         bstar !:Float32Array;
         dL !:Float32Array;
         da !:Float32Array;
         db !:Float32Array;
         dE !:Float32Array;
         Light_Stabilizer_Type !:string;
         Target_Light_Stabilizer_Content !:Float32Array;
         Actual_Light_Stabilizer_Content !:Float32Array;
         Antioxidant !:string;
         Antioxidant_Content !:Float32Array;
         Filler_Type !:string;
         Filler_Content !:Float32Array;
         Pigment_Type !:string;
         Pigment_Supplier !:string;
         Pigment_Content !:Float32Array;
         Impact_Modifier_Type !:string;
         Impact_Modifier_Content !:Float32Array;
         Processing_Location !:string;
         Mold !:string;
         Resin_Grade !:string;
      
}
