import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-jdcsummary',
  templateUrl: './jdcsummary.component.html',
  styleUrls: ['./jdcsummary.component.less']
})
export class JDCSummaryComponent implements OnInit {

  data:any;
  URL:any;
  SGID:any;
constructor(private http: HttpClient,private _global: GlobalService,private route: Router){
//get request from web api
this.URL=_global.URL;
this.http.get(this.URL+'api/GetJDCSummary/').subscribe(data => {

  this.data = data;
setTimeout(()=>{   
  $('#datatableexample').DataTable( {
    pagingType: 'full_numbers',
    pageLength: 10,
    processing: true,
    lengthMenu : [5, 10, 25],
    scrollX: true
} );
}, 1);
      }, error => console.error(error));

}   
deleteRow(d:any){
  const index = this.data.indexOf(d);
  var result = confirm("deleting Test ID will delete all records of this Test ID. Are sure you want to delete?");
  if (result) {
    let object = [];
    object.push(d.Test_ID);
  this.http.post(this.URL+'api/DeleteJDC',object).subscribe(data => {
          if(data=="1")
          {
            
           
           alert("Test ID Deleted successfully");
           window.location.reload();
          }
       
});
  }

}
   ngOnInit(){
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
    else{
      $('.dateadded').on( 'change', function (ret :any) {
 
         var v = ret.target.value  // getting search input value
         
         $('#dataTables-example').DataTable().columns(3).search(v).draw();
     } );
   
  }

}
}

