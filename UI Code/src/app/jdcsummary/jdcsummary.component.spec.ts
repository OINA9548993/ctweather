import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JDCSummaryComponent } from './jdcsummary.component';

describe('JDCSummaryComponent', () => {
  let component: JDCSummaryComponent;
  let fixture: ComponentFixture<JDCSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JDCSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JDCSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
