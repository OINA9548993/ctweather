import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicdatabaseSummaryComponent } from './micdatabase-summary.component';

describe('MicdatabaseSummaryComponent', () => {
  let component: MicdatabaseSummaryComponent;
  let fixture: ComponentFixture<MicdatabaseSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicdatabaseSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MicdatabaseSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
