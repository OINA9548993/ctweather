import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-micdatabase-summary',
  templateUrl: './micdatabase-summary.component.html',
  styleUrls: ['./micdatabase-summary.component.less']
})
export class MicdatabaseSummaryComponent implements OnInit {

 
  data:any;
  URL:any;
  SGID:any;
  constructor(private http: HttpClient,private _global: GlobalService,private route: Router){
  //get request from web api
  this.URL=_global.URL;
  this.http.get(this.URL+'api/GetMICSummary/').subscribe(data => {
  
    this.data = data;
  setTimeout(()=>{   
    $('#datatableexample').DataTable( {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      lengthMenu : [5, 10, 25],
      scrollX: true
  } );
  }, 1);
        }, error => console.error(error));
  
  }   
  
     ngOnInit(){
      this.SGID=sessionStorage.getItem("SGID")?.toString();
      if(this.SGID==undefined)
      {
        this.route.navigate(['/login']);
      }
      else{
        $('.dateadded').on( 'change', function (ret :any) {
   
           var v = ret.target.value  // getting search input value
           
           $('#dataTables-example').DataTable().columns(3).search(v).draw();
       } );
      }
     
    }
    deleteRow(d:any){
      debugger;
      const index = this.data.indexOf(d);
      var result = confirm("deleting Test ID will delete all records of this Test ID. Are sure you want to delete?");
  if (result) {
      let object = [];
      object.push(d.Formulation);
      object.push(d.Pigment);
      this.http.post(this.URL+'/api/DeleteMIC',object ).subscribe(data => {
              if(data=="1")
              {
               
                this.data.splice(index, 1);
                var a =$("#datatableexample_info")[0].innerHTML.substring(
                  $("#datatableexample_info")[0].innerHTML.indexOf("f") + 1, 
                  $("#datatableexample_info")[0].innerHTML.lastIndexOf("en")
              ).replace(',','');
              console.log($("#datatableexample_info")[0].innerHTML)
            //  alert(a);
              a=(Number(a)-1).toString();
              //  window.location.reload();
              $("#datatableexample_info")[0].innerHTML='Showing 1 to 10 of '+a+' entries';
              setTimeout(() => {
                alert("Row deleted successfully");
              }, 2000);
            
              }
    });
  }
  }
    


}
