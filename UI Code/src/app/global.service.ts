import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  pathPrefix: string = window.location.host;
  URL: string | undefined;
  constructor() { 

    if(this.pathPrefix=="localhost:4200")
    {
this.URL='http://localhost:54100/';
    }
    else{
//this.URL='https://digital-team-825wb.certainteed.com/weatherapp/';
this.URL='https://weatherapp.certainteed.com/';
    }
  }
}
