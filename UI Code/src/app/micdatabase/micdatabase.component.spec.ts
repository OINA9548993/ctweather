import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicdatabaseComponent } from './micdatabase.component';

describe('MicdatabaseComponent', () => {
  let component: MicdatabaseComponent;
  let fixture: ComponentFixture<MicdatabaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicdatabaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MicdatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
