import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnyRecordWithTtl } from 'dns';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-micdatabase',
  templateUrl: './micdatabase.component.html',
  styleUrls: ['./micdatabase.component.less']
})
export class MicdatabaseComponent implements OnInit {

  data:any;
  URL:any;
  SGID:any;
  pagesize:any;
pagenumber:any;
search:any;
dtOptions: DataTables.Settings = {};
persons: any;
dtTrigger: Subject<any> = new Subject();
  constructor(private http: HttpClient,private _global: GlobalService,private route: Router){

    this.URL=_global.URL;

        
  
  }   
  
     ngOnInit(){
      $.fn.dataTable.ext.errMode = 'none';
      const firstTime = localStorage.getItem('key')
      if(!firstTime){
       localStorage.setItem('key','loaded')
       location.reload()
      }else {
        localStorage.removeItem('key') 
      }
      const that = this;
      var datatableInstance = $('#datatableexample').DataTable({
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        searching: true,
        processing: true,
        fixedHeader: true,
        ajax: (dataTablesParameters: any, callback) => {
          $('#datatableexample thead tr:eq(1) th').find('input').each(function (i) {
            
            dataTablesParameters.columns[i].search.value=this.value;
          
          });
          that.http
            .post(
              this.URL+'/api/micdatabase/',
               dataTablesParameters
            ).subscribe((resp:any) => {
             // resp=JSON.stringify(resp)
             var jsonArray = JSON.parse(JSON.stringify(resp))
              this.persons=jsonArray.data ;
             // that.persons = resp.data;
      
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsFiltered,
                data: []
              });
            });
        },
        "language": {
          "search": "",
          "searchPlaceholder": "Search..."
      },
        columns: [
          { data: 'ID' }, 
          { data: 'Formulation' }, 
          { data: 'Pigment' },
          { data: 'mic.Date' , 'render': function ( data, type, row ) {
            return moment(data).format("MM-DD-YYYY HH:mm");
        } },
          { data: 'mic.duplicate' },
          { data: 'mic.Hours' },
          { data: 'mic.Lstar' },
          { data: 'mic.astar' },
          { data: 'mic.bstar' },
          { data: 'mic.dL' ,
          'render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { data: 'mic.da' ,
          'render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { data: 'mic.db',
          'render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { data: 'mic.dE' ,
          'render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { data: 'mic.Light_Stabilizer_Type' },
          { data: 'mic.Target_Light_Stabilizer_Content' },
          { data: 'mic.Actual_Light_Stabilizer_Content' ,
          'render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { data: 'mic.Antioxidant' },
          { data: 'mic.Antioxidant_Content' },
         { data: 'mic.Filler_Type' },
         { data: 'mic.Filler_Content' },
         { data: 'mic.Pigment_Type' },
          { data: 'mic.Pigment_Supplier' },
          { data: 'mic.Pigment_Content' },
          { data: 'mic.Impact_Modifier_Type' },
          { data: 'mic.Impact_Modifier_Content' },
         { data: 'mic.Processing_Location' },
         { data: 'mic.Mold' },
         { data: 'mic.Resin_Grade' },
          { data: 'mic.unique_ID' },
          { data: 'mic.Compound' },
          { data: 'mic.Compound_Lot' },
          { data: 'mic.Press' },
          { data: 'mic.Production_Date' },
          { data: 'mic.Notes' },
        ],
        
      });
      $('#datatableexample thead tr:eq(1) th').each(function () {
      
        var title = $('#datatableexample thead th').eq($(this).index()).text();
        if(title!="Delete")
        {
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    });

    
    datatableInstance.columns().every(function (index) {
      debugger;
      var that = this;
     
        var dataTableColumn = this;
        
        var colname= this.header().textContent ;

   
        $(this.header()).find('input').on('keyup change', function () {
        
          //  dataTableColumn.search(this.value);
            dataTableColumn.draw();
        });
      
    });
    
      this.SGID=sessionStorage.getItem("SGID")?.toString();
      if(this.SGID==undefined)
      {
        this.route.navigate(['/login']);
      }
      else{
    //  $("#loader").css("display", "none");
        $('.dateadded').on( 'change', function (ret :any) {
   
           var v = ret.target.value  // getting search input value
           
           $('#dataTables-example').DataTable().columns(3).search(v).draw();
       } );
      }
    }
    deleteRow(d:any){
      debugger;
      const index = this.data.indexOf(d);
      let object = [];
      object.push(d.unique_ID);
      this.http.post(this.URL+'/api/DeleteMIC',object ).subscribe(data => {
              if(data=="1")
              {
               
                this.data.splice(index, 1);
                var a =$("#datatableexample_info")[0].innerHTML.substring(
                  $("#datatableexample_info")[0].innerHTML.indexOf("f") + 1, 
                  $("#datatableexample_info")[0].innerHTML.lastIndexOf("en")
              ).replace(',','');
              console.log($("#datatableexample_info")[0].innerHTML)
            //  alert(a);
              a=(Number(a)-1).toString();
              //  window.location.reload();
              $("#datatableexample_info")[0].innerHTML='Showing 1 to 10 of '+a+' entries';
              setTimeout(() => {
                alert("Row deleted successfully");
              }, 2000);
            
              }
    });
   
  }
    

}
