import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {
  URL: string | undefined;
  data:any;
  SGID:any;
  pathPrefix:any;
  constructor(private http: HttpClient,private _global: GlobalService,private route: Router) {
    this.URL=_global.URL;
    this.pathPrefix=_global.pathPrefix;
    this.http.get(this.URL+'api/GetUser/').subscribe(data => {

      this.data = data;
    setTimeout(()=>{   
      $('#datatableexample').DataTable( {
        pagingType: 'full_numbers',
        pageLength: 10,
        processing: true,
        lengthMenu : [5, 10, 25],
        scrollX: true
    } );
    }, 1);
          }, error => console.error(error));
   }

  ngOnInit(): void {
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
  
  }

  editUser(d:any)
  {
    debugger;
    this.route.navigate(['/Add User', { id: d.SGI  }]);
   // window.location.href = "http://"+this.pathPrefix+"/Add User/"+d.SGI ;;
  }
  Delete(d:any)
  {
    debugger;
    const index = this.data.indexOf(d);
    var status=false;
    if(d.Status=="DeActivate")
    {
      status=true;
    }
    else{
      status=false;
    }
 
    this.http.get(this.URL+'api/DeleteUser/'+d.SGI.toString()+"/"+status).subscribe(data => {
            if(data=="1")
            {
              
              
             alert("User Updated Successfully");
             $.fn.dataTable.ext.errMode = 'none';
             this.http.get(this.URL+'api/GetUser/').subscribe(data => {

              this.data = data;
            setTimeout(()=>{   
              $('#datatableexample').DataTable( {
                pagingType: 'full_numbers',
                pageLength: 10,
                processing: true,
                lengthMenu : [5, 10, 25],
                scrollX: true
            } );
            }, 1);
                  }, error => console.error(error));
            }
  });
  
  }
}
