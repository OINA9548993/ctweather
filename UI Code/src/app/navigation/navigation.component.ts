import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit {

  navdropdown: boolean = false;
  Name=sessionStorage.getItem("Name")?.toString();
  Role="{"+sessionStorage.getItem("Role")?.toString()+"}";
  userRole:any;
  URL:any;
  dropdowntoggle(){
    this.navdropdown = !this.navdropdown
  }
  constructor(private route: Router,private http: HttpClient,private _global: GlobalService,private cookieService: CookieService) {
    this.URL=_global.URL;
   }

  navList: any [] = [
    {
      "name": "Data Upload",
      "icon": "fa-home"
    },
    {
      "name": "MIC Data",
      "icon": "fa-database"
    },
    {
      "name": "JDC Data",
      "icon": "fa-database"
    },
    {
      "name": "Admin",
      "icon": "fa-group"
    },
   
  ]
  
  DataUploadList: any [] = [
    {
      "name": "Preview Data",
      "icon": "fa-home"
    },
    {
      "name": "JDC Database",
      "icon": "fa-database"
    },
    {
      "name": "JDC Database Summary",
      "icon": "fa-file"
    },
    {
      "name": "JDC Test Conditions",
      "icon": "fa-group"
    },
    {
      "name": "MIC Database",
      "icon": "fa-database"
    },
    {
      "name": "MIC Database Summary",
      "icon": "fa-file"
    },
  ] 

  AdminList: any [] = [
    {
      "name": "Add User",
      "icon": "fa-plus"
    },
    {
      "name": "Manage User",
      "icon": "fa-edit"
    },
  
  ] 

  
  ngOnInit(): void {
  
    this.userRole=sessionStorage.getItem("Role")?.toString();
  //  alert(this.userRole);
    if(this.userRole=="Read")
    {
    var index = this.navList.findIndex(el => el.name === 'Data Upload');
if (index > -1) {
  this.navList.splice(index, 1);
}
index = this.navList.findIndex(el => el.name === 'Admin');
if (index > -1) {
  this.navList.splice(index, 1);
}

}
else if(this.userRole=="Write")
{
 var index = this.navList.findIndex(el => el.name === 'Admin');
if (index > -1) {
  this.navList.splice(index, 1);
}
    }
  }
Logout()
{
 
  this.cookieService.deleteAll();
  sessionStorage.clear();
  localStorage.clear();
  window.location.href = "https://uat.websso.saint-gobain.com/cas/logout"+ "?service=" + this.URL+"api/Logout/" ;;

 

 
}
}
