import { Component, OnInit } from '@angular/core';
import{CtweatherService } from '../ctweather.service'
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../global.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.less']
})
export class AuthorizeComponent implements OnInit {

  results:any;
  orderby!: string;
  pathPrefix:any;
  URL:any;
  result:any;
  constructor( private http: HttpClient,private CtweatherService:CtweatherService,private route: ActivatedRoute,private _global: GlobalService) { 
this.URL=_global.URL;
    this.pathPrefix=_global.pathPrefix;
  }

  ngOnInit() {
  
    this.route.queryParams
    .subscribe(params => {
      console.log(params);
      
      //this.orderby=params["SGID"]; 
     // this.Authenticate(this.orderby);
     sessionStorage.setItem("SGID",params["SGID"]);
     sessionStorage.setItem("Name",params["Name"]);
     this.http.get(this.URL+'api/GetUserRoleBySGID/'+params["SGID"]).subscribe(data => {
       console.log(data);
      this.result=data;
      sessionStorage.setItem("Role",this.result[0]["Role"]);
    // window.location.href = "https://digital-team-825wb.certainteed.com/weather/dashboard" ;
   // window.location.href = "http://"+this.pathPrefix+"/dashboard" ;;
   //window.location.href = "http://localhost:4200/dashboard" ;
    window.location.href = "https://weather.certainteed.com/weather/dashboard"
     });
    //  console.log(this.orderby);

    

    }
  );
  }
  Authenticate(ticket: string){
    this.CtweatherService.Auth(ticket).subscribe(data =>{ 
      this.results=data;
      console.log(this.results);
    });
  }
}
