import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { JDCDatabaseComponent } from './JDCDatabase/JDCDatabase.component';
import { PreviewComponent } from './preview/preview.component';
import { MICDataComponent } from './micdata/micdata.component';
import { MatTabsModule } from '@angular/material/tabs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AuthorizeComponent } from './authorize/authorize.component';
import { JDCSummaryComponent } from './jdcsummary/jdcsummary.component';
import { JdctestconditionComponent } from './jdctestcondition/jdctestcondition.component';
import { MicdatabaseComponent } from './micdatabase/micdatabase.component';
import { MicdatabaseSummaryComponent } from './micdatabase-summary/micdatabase-summary.component';
import { JdcdataComponent } from './jdcdata/jdcdata.component';
import { LogoutComponent } from './logout/logout.component';
import { ERROComponent } from './erro/erro.component';
import { AdminComponent } from './admin/admin.component';
import { AdduserComponent } from './adduser/adduser.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { CookieService } from 'ngx-cookie-service';
import { TestComponent } from './test/test.component';

//import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    JDCDatabaseComponent,
    PreviewComponent,
    MICDataComponent,
    AuthorizeComponent,
    JDCSummaryComponent,
    JdctestconditionComponent,
    MicdatabaseComponent,
    MicdatabaseSummaryComponent,
    JdcdataComponent,
    LogoutComponent,
    ERROComponent,
    AdminComponent,
    AdduserComponent,
    TestComponent,
    //TestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DataTablesModule,
    NgbModule,
    MatTabsModule,
    HttpClientModule,
    AutocompleteLibModule,
    MatAutocompleteModule,
      MatInputModule,
      FormsModule,
      ReactiveFormsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
