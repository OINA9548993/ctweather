import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ERROComponent } from './erro.component';

describe('ERROComponent', () => {
  let component: ERROComponent;
  let fixture: ComponentFixture<ERROComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ERROComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ERROComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
