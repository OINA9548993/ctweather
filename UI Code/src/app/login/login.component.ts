import { Component, OnInit } from '@angular/core';
import{CtweatherService } from '../ctweather.service'
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  results:any;
  orderby!: string;
  SGID:string | undefined;
  URl:any;
  pathPrefix:any;
  constructor(private CtweatherService:CtweatherService,private route: ActivatedRoute,private _global: GlobalService) { 
    this.URl=_global.URL;
    this.pathPrefix=_global.pathPrefix;
  }

  ngOnInit() {
   this.SGID=sessionStorage.getItem("SGID")?.toString();
   if(this.SGID!==undefined)
   {
     console.log(this.pathPrefix);
//window.location.href = "http://"+this.pathPrefix+"/dashboard" ;;
//window.location.href = "http://localhost:4200/dashboard" ;
 // window.location.href = "https://digital-team-825wb.certainteed.com/weather/dashboard" ;;
  window.location.href = "weather.certainteed.com/weather/dashboard" ;;
   }
   else if (this.SGID=="") {
    window.location.href = "http://"+this.pathPrefix+"/login" ;;
    sessionStorage.removeItem('SGID');
    sessionStorage.removeItem('Name');
   }
   else{
   $('.user-profile').remove();
   $('.navigation').remove();
   $('.footer').remove();
   $('.container-fluid').remove();
   // window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + this.URl+"api/sso/" ;;
   window.location.href = "https://websso.saint-gobain.com/cas/login"+ "?service=" + this.URl+"api/sso/" ;
  }

}

}
