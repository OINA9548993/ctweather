export class User {
  id!: number;
  firstName!: string;
  lastName!: string;
  isAdmin: boolean = false;
  SGID!: string;
  // siteId: number;
}
