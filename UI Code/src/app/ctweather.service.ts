import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Jdc } from './jdc';
import { MIC } from './mic';
import { User } from './user.model';
import { MICExcel } from './micexcel';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class CtweatherService {

  endPoint: string | undefined; 
  URl:any;
  pathPrefix:any; 
  constructor(private http: HttpClient,private _global: GlobalService) { 
    this.URl=_global.URL;
    this.pathPrefix=_global.pathPrefix;
  }
  getAllEmployee(): Observable<Jdc[]> {  
    return this.http.get<Jdc[]>(this.URl + 'api/Gettabledata');  
  }  

  getGraphData(Ids:string): Observable<MIC[]> {  
    return this.http.get<MIC[]>(this.URl + 'api/GetGraphdata/'+Ids,  {
      headers:new HttpHeaders({
       
        'Content-Type': 'application/json',
        
      })
      });    
  } 
  getJDCGraphData(Ids:string): Observable<Jdc[]> {  
    return this.http.post<Jdc[]>(this.URl + 'api/GetJDCGraphdata/'+Ids,  {
      headers:new HttpHeaders()
      .set('Content-Type','application/json')
      });   
  }  
 

  Auth(ticket: string): Observable<User[]> {  
    let httpParams = new HttpParams();
    
    httpParams = httpParams.append('ticket', ticket);
    return this.http.get<User[]>(this.URl + 'api/sso', { params: httpParams });  
  } 
  
  private _getHeaders():Headers {
    let header = new Headers({
      'Content-Type': 'application/json'
    });
 
    return header;
 }
 search(name: string): Observable<any[]> {  
  return this.http.post<any[]>(this.URl + 'api/GetAllUser/'+name,  {
    headers:new HttpHeaders()
    .set('Content-Type','application/json')
    });    
}  

  MICExcel(arraylist:any): Observable<MICExcel[]> {  
 
    let httpParams = new HttpParams();
    debugger;
    httpParams = httpParams.append('ticket',arraylist);
    return this.http.post<MICExcel[]>(this.URl + 'api/Excel',arraylist,  {
      headers:new HttpHeaders()
      .set('Content-Type','application/json')
      });  
  }
}
