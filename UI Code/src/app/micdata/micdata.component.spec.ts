import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MICDataComponent } from './micdata.component';

describe('MICDataComponent', () => {
  let component: MICDataComponent;
  let fixture: ComponentFixture<MICDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MICDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MICDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
