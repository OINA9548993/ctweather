import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizeComponent } from './authorize/authorize.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MICDataComponent } from './micdata/micdata.component';
import { PreviewComponent } from './preview/preview.component';
import { JDCDatabaseComponent } from './JDCDatabase/JDCDatabase.component';
import { JDCSummaryComponent } from './jdcsummary/jdcsummary.component';
import { JdctestconditionComponent } from './jdctestcondition/jdctestcondition.component';
import { MicdatabaseComponent } from './micdatabase/micdatabase.component';
import { MicdatabaseSummaryComponent } from './micdatabase-summary/micdatabase-summary.component';
import { JdcdataComponent } from './jdcdata/jdcdata.component';
import { LogoutComponent } from './logout/logout.component';
import { ERROComponent } from './erro/erro.component';
import { AdminComponent } from './admin/admin.component';
import { AdduserComponent } from './adduser/adduser.component';
import { TestComponent } from './test/test.component';



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent  },
  { path: 'Logout', component: LogoutComponent  },
  { path: 'authorize', component: AuthorizeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'JDC Database', component: JDCDatabaseComponent },
  { path: 'JDC Database Summary', component: JDCSummaryComponent },
  { path: 'JDC Test Conditions', component: JdctestconditionComponent },
  { path: 'Preview Data', component: PreviewComponent },
  { path: 'MIC Data', component: MICDataComponent },
  { path: 'JDC Data', component: JdcdataComponent },
  { path: 'MIC Database', component: MicdatabaseComponent },
  { path: 'MIC Database Summary', component: MicdatabaseSummaryComponent },
  { path: 'Error', component: ERROComponent },
  { path: 'Add User', component: AdduserComponent },
  { path: 'Manage User', component: AdminComponent },
  { path: 'test', component: TestComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
}
