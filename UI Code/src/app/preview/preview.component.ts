import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import{CtweatherService } from '../ctweather.service';
import {ActivatedRoute, Router} from '@angular/router';
import{AppComponent} from '../app.component';
import { GlobalService } from '../global.service';

type AOA = any[][];
@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.less']
})
export class PreviewComponent implements OnInit {
  @ViewChild('fileInput') fileInput: any; 
  data: AOA = [[1, 2], [3, 4]];
  datarow: AOA = [[1, 2], [3, 4]];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  file: any;
  arrayBuffer:any;
  filelist!: never[];
  message: string | undefined;  
  url:any;
visibility:any=[];
visibilityID:any=[];
selectedvalue:any="";
Importeddata:any;
coloumn:any;
AllData:any=[];
SGID:any;
isButtonVisible = false;
MIC = false;
JDC = false;
Show=false;
test_id:any;
//private router = ActivatedRoute;

  constructor(private CtweatherService:CtweatherService,private http: HttpClient,private router:Router,private _global: GlobalService,private route: Router) { 

    this.url=_global.URL;
  }

  ngOnInit(): void {
    debugger;
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
    else{
    $("#loader").css("display", "none");
    }
  }

  updateData(){
    if(this.selectedvalue=="JDC")
    {
      let object = [];
      object.push(this.test_id);
   
      this.http.post(this.url + 'api/updateJDCData',object).subscribe(data => {
        if(data=="Success"){
        alert('Data Uploaded successfully')
        window.location.reload();
        }
        else
        {
          alert(data);
        }
      });
    }
    else{
    this.http.get(this.url + 'api/updateData').subscribe(data => {
      if(data=="Success"){
      alert('Data Uploaded successfully')
      window.location.reload();
      }
      else
      {
        alert(data);
      }
    });
  }
  }

  onFileChange(evt: any) {

    
    if(this.selectedvalue=="")
    {
alert("Please select file template and upload again");
this.Show=false;


this.fileInput.nativeElement.value = '';

return;
    }
    else{
      $("#loader").css("display", "block");
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = async (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary',cellText: false, cellDates: true });
      const wsname: string = wb.SheetNames[0];
     
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
   
      /* grab first sheet */
//alert(this.selectedvalue);
      if(this.selectedvalue=="JDC")
      {
     //   alert("in JDc");
        this.JDC=true;
      this.MIC=false;
        debugger;

       let formData = new FormData();  
       formData.append('upload', target.files[0])  
        
          let headers = new HttpHeaders();  
         
            headers.append('Content-Type', 'multipart/form-data');  
            headers.append('Accept', 'application/json');
            headers.append("Access-Control-Allow-Origin", "*");
           this.http.post<any>(this.url + 'api/JDCExcelData',formData , { headers }).subscribe(data => {
        
             console.log(data);
            // var a:string=data;
             if(data.indexOf("success")<0)
   {
    let link = document.createElement("a");
    link.download = "JDCExcel";
    link.href = "assets/JDCExcel.xlsx";
    link.click();
    $("#loader").css("display", "none");
    alert(data + ' and see downloaded file for reference')
    this.Show=false;
    this.fileInput.nativeElement.value = '';
    return;
   }else{
    this.test_id=data.replace('success ','')
    this.http.get(this.url + 'api/GetExcelImportedData/'+this.selectedvalue.toString()).subscribe(data => {
      this.Show=true;
      this.Importeddata=data;
      if(this.Importeddata.length==0)
      {
        this.isButtonVisible = false;
        $("#loader").css("display", "none");
       alert('All Data in this file already Compiled');
    
      }
      else
      {
        this.isButtonVisible = true;
        $("#loader").css("display", "none");
       alert('Data Compiled successfully');
       
      }
      setTimeout(()=>{   
       $('#datatableexample').DataTable( {
         pagingType: 'full_numbers',
         pageLength: 10,
         processing: true,
         lengthMenu : [5, 10, 25],
         scrollX: true
     } );
     }, 1);
   
     });
    
  
   }

           });
      }
    else{
     // alert("in MIC");
      this.JDC=false;
      this.MIC=true;
      debugger;
      wb.Workbook?.Sheets?.map((x) => { 
        if(x.Hidden==1)
        {
        this.visibility.push(x.name);
       
        }
        else{
          this.visibilityID.push(x.name);
        }
        
        });

        var error=false;
        var messageerror;
        if(wb.SheetNames.length<4)
        {
          let link = document.createElement("a");
      link.download = "MICExcel";
      link.href = "assets/MICExcel.xlsx";
      link.click();
      $("#loader").css("display", "none");
      alert('Please upload valid excel sheet and see downloaded file for reference')
      this.Show=false;
      this.fileInput.nativeElement.value = '';
      return;
        }else{
        for(var i=3;i<wb.SheetNames.length;i++)
  {
    if(error==false){
    if(this.visibility.indexOf( wb.SheetNames[i]) > -1==false){
    var wsname2: string = wb.SheetNames[i];
     
    var ws2: XLSX.WorkSheet = wb.Sheets[wsname2];
   this.datarow = <AOA>XLSX.utils.sheet_to_json(ws2, { header: 1 ,raw:false,dateNF:'yyyy-mm-dd'},);
 
    let object = []
  object.push(wsname2);
  object.push(this.datarow);

    let headers = new HttpHeaders();  
 
     headers.append('Content-Type', 'multipart/form-data');  
     headers.append('Accept', 'application/json');
  this.http.post<any>(this.url + 'api/Excel',object , { headers }).subscribe(data => {

   //console.log(data);
   if(data!="success")
   {
error=true;
messageerror=data;

 return;
   }
  
   });
  }
}
else
{
  let link = document.createElement("a");
 link.download = "MICExcel";
 link.href = "assets/MICExcel.xlsx";
 link.click();
 $("#loader").css("display", "none");
 alert(messageerror + ' and see downloaded file for reference')
 this.Show=false;
 this.fileInput.nativeElement.value = '';
  break;
}
  }
  setTimeout(() => {
  const wsname: string = wb.SheetNames[0];
     
  const ws: XLSX.WorkSheet = wb.Sheets[wsname];

  /* save data */
  this.data = <AOA>XLSX.utils.sheet_to_json(ws, { header: 1 ,raw:false,dateNF:'yyyy-mm-dd'});
 
  let object = []
  object.push(wsname);
  object.push(this.data);
  object.push(this.visibilityID);
  
  
    let headers = new HttpHeaders();  
   
      headers.append('Content-Type', 'multipart/form-data');  
      headers.append('Accept', 'application/json');
     this.http.post<any>(this.url + 'api/ExcelData',object , { headers }).subscribe(data => {
  
      console.log(data);
      if(data!="Sucess")
      {

        let link = document.createElement("a");
    link.download = "MICExcel";
    link.href = "assets/MICExcel.xlsx";
    link.click();
    $("#loader").css("display", "none");
    alert(data + ' and see downloaded file for reference')
    this.Show=false;
    this.fileInput.nativeElement.value = '';
    return;
      }
      else
      {
        this.http.get(this.url + 'api/ImpportfromExcel').subscribe(data => {
          if(data=="Success"){
          
        this.http.get(this.url + 'api/GetExcelImportedData/'+this.selectedvalue.toString()).subscribe(data => {
          this.Show=true;
         this.Importeddata=data;
         if(this.Importeddata.length==0)
         {
           this.isButtonVisible = false;
           $("#loader").css("display", "none");
          alert('All Data in this file already Compiled');
       
         }
         else
         {
          $("#loader").css("display", "none");
          alert('Data Compiled successfully')
          this.isButtonVisible = true;
          
         }
         setTimeout(()=>{   
          $('#datatableexample').DataTable( {
            pagingType: 'full_numbers',
            pageLength: 10,
            processing: true,
            lengthMenu : [5, 10, 25],
            scrollX: true
        } );
        }, 1);
        
        });
      
      }
      else{
        alert(data);
        return;
      }
       
  });
      }
    });
  }, 7000);

    return;

        }

      
    };
  }

  
  
    reader.readAsBinaryString(target.files[0]);
  
    }
  }

  

  uploadtoDatabase(body: any,headers: any)
  {
    this.http.post<any>(this.url + 'api/Excel', body, { headers }).subscribe(data => {
      console.log(data);
    });
  }
 
  Onchange(e:any) {
    console.log(e.target.value);


this.selectedvalue=e.target.value;
if(this.selectedvalue=="JDC")
{
  $("#t1").css("display", "none");
  $("#t2").css("display", "block");
}
else{
  $("#t2").css("display", "none");
  $("#t1").css("display", "block");
}

  }






}
