import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { data } from 'jquery';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap,of, startWith, map, tap, finalize } from 'rxjs';  
import{CtweatherService } from '../ctweather.service'
import { GlobalService } from '../global.service';
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.less']
})
export class AdduserComponent implements OnInit {
  searchMoviesCtrl = new UntypedFormControl();
  filteredMovies: any;
  isLoading = false;
  errorMsg: string | undefined;
  Role:string | undefined;
  URL: string | undefined;
  result:any;
  Roleid:any;
  userRole:any;
  SGID:any;
  constructor(
    private http: HttpClient,private _global: GlobalService,private route: Router
  ) {  this.URL=_global.URL;}

  ngOnInit() {
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID=="undefined")
    {
      this.route.navigate(['/Add User']);
    }
    else{
    this.userRole=sessionStorage.getItem("Role")?.toString();
   var SGID1 = this.route.routerState.snapshot.url;
 
   if(SGID1.includes("id")==false)
   {
    if(this.searchMoviesCtrl.value!="")
    
    this.searchMoviesCtrl.valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.errorMsg = "";
          this.filteredMovies;
          this.isLoading = true;
        }),
        switchMap(value => 
          this.http.get(this.URL + "api/GetAllUser/" + value)
          .pipe(
            finalize(() => {
              this.isLoading = false
            }),
          )
        )
      )
      .subscribe(data => {
       
      //  if (data['Search'] == undefined) {
       //   this.errorMsg = data['Error'];
        //  this.filteredMovies = [];
       // } else {
       //   this.errorMsg = "";
       console.log(data);
          this.filteredMovies = data;
       // }

        console.log(this.filteredMovies);
      },(error) => {                              //Error callback
        console.error('error caught in component')
        
  
        //throw error;   //You can also throw the error to a global error handler
      });
    }
    else
    {
      $("#upload").text("Edit User");
      var SGID=SGID1.substring(SGID1.lastIndexOf("=")+1)
     // alert(SGID);
      
      this.http.get(this.URL+'api/GetUserBySGID/'+SGID).subscribe(data => {
        this.result=data;
       
//$("#flexRadioDefault"+this.result[0]["SiteAdmin"]).attr("checked", true);
        $("#flexRadioDefault"+this.result[0]["SiteAdmin"]).prop("checked", true);
      
        //this.searchMoviesCtrl=this.result[0]["displayName"];
        $("#Name").text(this.result[0]["displayName"]);
        $("#Search").css("display", "none");
        $(".mat-form-field").css("display", "none");
        console.log(data);
      });
    }
  }
  }

  onItemChange(value: any){
    this.Role=value.target.value;
    console.log(" Value is : ", value );
 }
  Adduser()
  {
    debugger;
    var SGID1 = this.route.routerState.snapshot.url;
    if(SGID1.includes("id")==false)
   {
    let object = []
   
      object.push(this.searchMoviesCtrl.value);
      object.push(this.Role);
      
        let headers = new HttpHeaders();  
     
         headers.append('Content-Type', 'multipart/form-data');  
         headers.append('Accept', 'application/json');
    var val = {Name:this.searchMoviesCtrl.value,
      Role:this.Role,
      };
      this.http.post(this.URL + 'api/InsertUser', object, { headers }).subscribe(res =>{
        alert(res.toString());
      })

      
  }
  else
  {
    let object = []
    var SGID1 = this.route.routerState.snapshot.url;
    var SGID=SGID1.substring(SGID1.lastIndexOf("=")+1)
    object.push($("#Name").text());
    object.push(this.Role);
    object.push(SGID);
      let headers = new HttpHeaders();  
   
       headers.append('Content-Type', 'multipart/form-data');  
       headers.append('Accept', 'application/json');

    this.http.post(this.URL + 'api/EditUser', object, { headers }).subscribe(res =>{
      alert(res.toString());
    })
  }
}
}