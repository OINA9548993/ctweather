import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import * as XLSX from 'xlsx';
import * as fileSaver from 'file-saver';
//import { Chart } from 'chart.js';
import { title } from 'datatables.net-editor/types/core/api';
import { couldStartTrivia } from 'typescript';
import{CtweatherService } from '../ctweather.service'
import { GlobalService } from '../global.service';
//import {CategoryScale} from 'chart.js'; 
import { Chart, registerables } from 'chart.js';
@Component({
  selector: 'app-jdcdata',
  templateUrl: './jdcdata.component.html',
  styleUrls: ['./jdcdata.component.less']
})

export class JdcdataComponent implements OnInit {

  data1:any;
  Hours: any = [];
  dE: any  = [];
  dEs: any  = [];
  AlldEs: any  = [];
  Label:any=[];
  ids: any  = [];
  formulation: any  = [];
  color: any  = [];
  allDatasets:any;
  results:any;
  maxyvalue:any;
  maxxvalue:any;
  minxvalue:any;
  minyvalue:any;
  tr:any;
  chart:any=[];
  data:any;
  selected1:string="Formulation";
  selected2:string="Color";
  shape:any=["circle","cross","crossRot","line","rect","rectRounded","rectRot","star","triangle"];
  @ViewChild('canvasEl')
  canvasEl!: ElementRef;
  private context:any// CanvasRenderingContext2D;
  ColumnData: any;
  Datavalue:any="DE";
  labels:any="Hours";
  shapes:any =[];
  test:any =[];
  time_metric:any =[];
  time_metricval:any;
  time:any =[];
  graphcolor:any=["#AF7AC5","#DAF7A6","#FFC300","#FF5733","#C70039","#900C3F","#17202A","#5DADE2"];
    URL:any;
    SGID:any;
    
  constructor( private CtweatherService:CtweatherService,private elementRef: ElementRef,private http: HttpClient,private _global: GlobalService,private route: Router) { 
   
    this.URL=_global.URL;
    Chart.register(...registerables);
  }

  ngOnInit() {

    this.http.get(this.URL+'api/GetJDCSummary/').subscribe(data => {
      this.data1 = data;
      var datatableInstance = $('#datatableexample').DataTable({
        paging: true,
        ordering: false,
        fixedHeader: true,
        searching: true,
        data: this.data1 ,
        columns: [
          
          { 'data': 'Test_ID' },
          { 'data': 'test_type' },
          { 'data': 'test_location' },
         
          { 'data': 'Sample_code' },
          { 'data': 'test_location_id' },
          { 'data': 'color' },
          { 'data': 'Formulation' },
          { 'data': 'Sample_name' },
          { 'data': 'HBU','render': function ( data, type, row ) {
            return parseFloat(data).toFixed(2);
        } },
          { 'data': 'Gloss' },
          { 'data': 'Comments' },
        ],
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');
          $('td', row).bind('click', (event) => {
            this.selectrow(event);
          });
          return row;
        }
    });
    
    

    $('#datatableexample thead tr:eq(1) th').each(function () {
      
        var title = $('#datatableexample thead th').eq($(this).index()).text();
        if(title!="Delete")
        {
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    });
  //  $('.table tbody').on( 'click', 'tr', function (event:any) {
    
   //   event.currentTarget.classList.add("selected");
    //  this.formulation.push(event.currentTarget.children[6].innerHTML);
 // } );

    
    datatableInstance.columns().every(function (index) {

   
     
        var dataTableColumn = this;
        
        var colname= this.header().textContent ;
        if(index==0 || index==1 || index==3 || index==5 || index==9)
        {
          var select = $('<select><option value=""></option></select>')
      .appendTo( $(dataTableColumn.header()).empty());

      dataTableColumn.data().unique().sort().each( function ( d, j ){
        select.append( '<option value="' + d + '">' + d + '</option>' );
      });
      $(this.header()).find('select').on('change', function () {
        dataTableColumn.search(this.value).draw();
    });
        }
        else{
        $(this.header()).find('input').on('keyup change', function () {
            dataTableColumn.search(this.value).draw();
        });
      }
    });

    });
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
    else{
    $('.dateadded').on( 'change', function (ret :any) {
   
      var v = ret.target.value  // getting search input value
      
      $('#dataTables-example').DataTable().columns(3).search(v).draw();

  
  } );
 
  this.http.get(this.URL+'api/GetJDCColumnName/').subscribe(data => {
  
    this.ColumnData = data;
 
        }, error => console.error(error));
      }


  }
  ExportExcel()
  {

    var a: { toString: () => any; };
    //this.ids.push("1.1");
    //this.ids.push("2.1");
    let object: { toString: () => any; }[] = [];
  
    for (let i = 0; i < this.ids.length; i++) {
      a=this.ids[i];
      object.push(a);
        
        }
  
       
        setTimeout( ()=> { 
        
          this.http.post<any>(this.URL + 'api/GetJDCExceldata/',object).subscribe(data => {
          //  this.exportdata+=data;
           // this.ExportExcelData.push(data);
            const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
            const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
            this.saveAsExcelFile(excelBuffer, "JDCData");
          });
  
        },1000);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'});
    fileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + '.xlsx');
  }
  
  onOptionsSelected(value:string){
  //  alert("the selected value is " + value);
    this.Datavalue=value;
}

 getMax(arr:  any[], prop: string | number) {
  var a= Math.max(...arr.map(item => item[prop]))
  return a;
}

getMin(arr: any[], prop: string | number) {
 var a= Math.min(...arr.map(item => item[prop]))
 return a;
}
ClearRows(){
  $('#datatableexample tr').removeClass("selected");
  this.dE  = [];
  this.Hours  = [];
  this.ids=[];
  
  this.formulation  = [];
  this.color  = [];
  this.shapes  = [];
  this.shape  = [];

  this.dEs=[];
 
  this.Label=[];
}
  selectrow(event:any)
  {

    if (event.target.parentElement.classList.contains('selected')==true) {
     
      let index = this.formulation.indexOf(event.target.parentElement.children[6].innerHTML);
if (index > -1) {
  this.formulation.splice(index, 1); // 2nd parameter means remove one item only
}
index = this.color.indexOf(event.target.parentElement.children[5].innerHTML);
if (index > -1) {
  this.color.splice(index, 1); // 2nd parameter means remove one item only
}
index = this.ids.indexOf(event.target.parentElement.children[4].innerHTML);
if (index > -1) {
  this.ids.splice(index, 1); // 2nd parameter means remove one item only
}
index = this.shapes.indexOf(event.target.parentElement.children[7].innerHTML);
if (index > -1) {
  this.shapes.splice(index, 1); // 2nd parameter means remove one item only
}

event.target.parentElement.classList.remove("selected");
    }
    else{
      event.target.parentElement.classList.add("selected");
    this.formulation.push(event.target.parentElement.children[6].innerHTML);
    this.color.push(event.target.parentElement.children[5].innerHTML);
    this.shapes.push(event.target.parentElement.children[7].innerHTML);
    this.tr=$('.selected');
    this.ids  = [];
    for (let i = 0; i < this.tr.length; i++) {
      if(this.ids.indexOf($(this.tr[i]).find("td").eq(4).html()) == -1){
      this.ids.push($(this.tr[i]).find("td").eq(4).html());
      //this.formulation.push($(this.tr[i]).find("td").eq(6).html());
     // this.color.push($(this.tr[i]).find("td").eq(5).html());
     
      
      }
    }
  }
   // $(this).toggleClass('selected');
  }
async filldata()
{
  for (let i = 0; i < this.ids.length; i++) {
    var a=this.ids[i];
  
        this.CtweatherService.getJDCGraphData(a).subscribe(async data =>{ 
          this.results=data;
  
          this.Label.push(this.results[0][this.selected1]+"_"+this.results[0][this.selected2]);
          this.dE.push("0");
          this.test.push({x:0,y:0,time:this.time_metricval});
      // this.dE=   this.results.map((result:any) => ({ value: result.dE }));
      //  this.Hours=  this.results.map((result:any) => ({ value: result.Hours }));
      for (let j = 0; j < this.results.length; j++) {
      
        await  this.time_metric.push(this.results[j].Time_metric);
        await  this.time.push(this.results[j].Time);
        await  this.test.push({x: Math.round(this.results[j].Hours * 100) / 100, y: Math.round(this.results[j][this.Datavalue] * 100) / 100,time:this.results[j].Time_metric})
       
     }
     
     this.dEs.push(this.test);
     this.test=[];
   
    }); 
  
  }
}

async fillchangedata()
{

  for (let i = 0; i < this.ids.length; i++) {
    var a=this.ids[i];
  
        this.CtweatherService.getJDCGraphData(a).subscribe(async data =>{ 
          this.results=data;
          this.dE.push("0");
          this.test.push({x:0,y:0,time:this.time_metricval});
   for (let j = 0; j < this.results.length; j++) {
    
  if(this.results[j].Time_metric=="Months"){
    await  this.test.push({x: Math.round(this.results[j].Hours *24*30* 100) / 100, y: Math.round(this.results[j][this.Datavalue] * 100) / 100,time:this.time_metricval})
   }
   else
   {
    await   this.test.push({x: Math.round(this.results[j].Hours * 100) / 100, y: Math.round(this.results[j][this.Datavalue] * 100) / 100,time:this.time_metricval})
   }


  }

  await this.dEs.push(this.test);
  this.test=[];
  })
}

}
  async updateplot(){
  
    $("#chart").css("display", "none");
    if(this.ids.length==0){
      alert("please select rows from JDC Data");
      return;
      
      }else{
    this.dE  = [];
    this.Hours  = [];
    this.dEs=[];
 this.time_metric=[];
 this.time=[];
    this.Label=[];

    $("#loader").css("display", "block");
    var maxyarr: any[]=[];
    var minyarr: number[]=[];
    var maxxarr: any[]=[];
    var minxarr: any[]=[];
    this.maxyvalue='';
this.minyvalue='';
this.maxxvalue='';
this.minxvalue='';
 await this.filldata();
 setTimeout( async ()=> {  
 // console.log(this.time_metric);
  
   const result =this.time_metric.every( (val: any, i: any, arr: any[]) => val === arr[0] )  
  // console.log(result);
  // console.log(this.time);
   if(result==true)
   {
     this.time_metricval=this.time_metric[0];
   //  console.log(this.dEs);
   }
   else
   {
    this.dEs=[];
     this.time_metricval="Hours";
     for(var i=0;i<this.time_metric.length;i++)
     {
       if(this.time_metric[i]=="Months")
       {
         this.time[i]=24*30*this.time[i];
       }
     }
await this.fillchangedata();
   } 
   //console.log(this.dEs);
  },3000);
 //  alert(Math.floor(Math.random() * this.shape.length));

 setTimeout( async ()=> {   
   this.data = { labels: this.time, datasets: [] }
  // alert(this.dEs.length);
 for(var i=0;i<this.dEs.length;i++)
 {
this.data.datasets.push({
  type: 'scatter',
  label: this.Label[i],
  data:this.dEs[i],
  legendText: 'Color: '+this.color[i]+' ,Shape: '+this.shapes[i]+' , '+this.Datavalue,
  backgroundColor: 
  this.graphcolor[i],
      
  borderColor: 
  this.graphcolor[i],
     borderWidth: 3,
     fill:false,
     lineTension: 0, 
     pointStyle:this.shape[Math.floor(Math.random() * this.shape.length)],  
     pointRadius: 5,
     pointHoverRadius: 5,
     pointBackgroundColor: this.graphcolor[i],
     showLine: true,

});}
//console.log(this.results);
var a=this.dEs;
for(var i=0;i<this.dEs.length;i++)
{
  for(var k=0;k<this.dEs[i].length;k++){
await  this.AlldEs.push(this.dEs[i][k].y)
  }
}
//maxyarr.push(this.getMax(this.AlldEs,''));
 //  minyarr.push(this.getMin(this.AlldEs,''));
   maxxarr.push(Math.max.apply(null, this.time));
   minxarr.push(Math.min.apply(null, this.time));
this.maxyvalue=Math.max(...this.AlldEs)
this.minyvalue=Math.min(...this.AlldEs)
this.maxxvalue=Math.max(...maxxarr)
this.minxvalue=Math.min(...minxarr)
if(this.maxyvalue<1)
   {
    this.maxyvalue=this.maxyvalue+0.2
   }
  
   else if(this.maxyvalue<10)
   {
    this.maxyvalue=this.maxyvalue+1
   }
   else if(this.maxyvalue<50)
   {
    this.maxyvalue=this.maxyvalue+2
   }
   else if(this.maxyvalue<100)
   {
    this.maxyvalue=this.maxyvalue+50
   }
   else 
   {
    this.maxyvalue=this.maxyvalue+500
   }
  
if(this.minyvalue>0)
{
 this.minyvalue=0;
}
if(this.minxvalue>0)
{
 this.minxvalue=0;
}
if(this.maxxvalue<1)
{
 this.maxxvalue=this.maxxvalue+0.2
}
else if(this.maxxvalue<10)
{
 this.maxxvalue=this.maxxvalue+1
}
else if(this.maxxvalue<50)
{
 this.maxxvalue=this.maxxvalue+2
}
else if(this.maxxvalue<100)
{
 this.maxxvalue=this.maxxvalue+50
}
else 
{
 this.maxxvalue=this.maxxvalue+500
}
//alert(this.maxyvalue)
//alert(this.minyvalue)
},9000);

    setTimeout(() => {

$("#loader").css("display", "none");
      $("#chart").css("display", "block");
     this.Drawchart();
},10000)

      }
  }
  onTabChanged($event: { index: any; }) {
    $("#loader").css("display", "none");
  //  $("#chart").css("display", "none");
  }
  
  getRandomColor() {
    var color = Math.floor(0x1000000 * Math.random()).toString(16);
    return '#' + ('000000' + color).slice(-6);
  }
  Drawchart(){
  //console.log(this.data);
    this.context = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d')
   
  
    if (this.chart.ctx) {

      this.chart.destroy();
    }


    this.chart = new Chart(this.context, {
      type: 'scatter',
      data: this.data, 
      options: {
      
        plugins: {
        tooltip: {
          callbacks : {
            label: function(context) {
             // console.log(context.raw);
              var json_data:any = context.raw;
var result = [];

for(var i in json_data)
    result.push([ json_data [i]]);


              return  result[2] + ': ' + context.parsed.x ;
          },
          afterLabel: function(context) {
            var val=[];
            
val.push(context.chart.data.datasets[context.datasetIndex]);
var val1=val[0];
var value1='';
Object.entries(val1).forEach(([key, value], index) => {
 if(index==3){
value1=value;
 }
 // console.log(key, value, index);
});
//console.log(val[0]); 
            
            return   value1 + ': ' + context.parsed.y;
        },
          }
        },
        
        
        legend: {
       
          display: true,
          position: 'right',
            labels: {
              usePointStyle: true,
            },
            
        },
      },
        scales: {
          

          x: {
            beginAtZero: true,
            min:this.minxvalue,
              max:this.maxxvalue,
            //offset: true,
            title: {
              display: true,
              text: 'Time('+this.time_metricval+')'
            },
           
          },
          y: {
            beginAtZero: true,
              min:this.minyvalue,
              max:this.maxyvalue,
            title: {
              display: true,
              text: this.Datavalue
            },
          
          },
        },
      
      
      }
      
  });
 
  }


  onCheckboxChange()
  {
    if(this.maxyvalue<4)
    {
      this.maxyvalue=5
    }
    this.context = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d')
   
  
    if (this.chart.ctx) {

      this.chart.destroy();
    }

    const plugin = {
      id: 'custom_canvas_background_color',
      beforeDraw: (chart:any, args: any, options:any) => {
          const {ctx,scales:{x,y}} = chart;
          //console.log(chart.scales);
         // var yScale = chart.ctx.scales["x-axis-1"];
        // var yValue = yScale.getPixelForValue(4);
         var canvas = chart.ctx.canvas;
         
          ctx.save();
         // ctx.beginPath();
        //  ctx.moveTo(0, yValue);
         // ctx.lineTo(canvas.width, yValue);
          ctx.strokeStyle = 'Red';
          ctx.strokeRect(50,y.getPixelForValue(4),chart.canvas.width-180,0)
         // ctx.stroke();
      },
      defaults: {
          color: 'lightGreen'
      }
  }
    this.chart = new Chart(this.context, {
      type: 'scatter',
      data: this.data, 
      plugins:[plugin]
      ,
      options: {
        plugins: {
        
        tooltip: {
          callbacks : {
            label: function(context) {
              return  'Hours: ' + ': ' + context.parsed.x ;
          },
          afterLabel: function(context) {
            var val=[];
            
val.push(context.chart.data.datasets[context.datasetIndex]);
var val1=val[0];
var value1='';
Object.entries(val1).forEach(([key, value], index) => {
 if(index==3){
value1=value;
 }
//  console.log(key, value, index);
});
//console.log(val[0]); 
            
            return   value1 + ': ' + context.parsed.y;
        },
          }
        },
        
        
        legend: {
       
          display: true,
          position: 'right',
            labels: {
              usePointStyle: true,
            },
            
        },
        
        
      },
        scales: {
          

          x: {
            beginAtZero: true,
            min:0,
              max:this.maxxvalue,
            //offset: true,
            title: {
              display: true,
              text: 'Time(hours)'
            },
           
          },
          y: {
            beginAtZero: true,
              min:0,
              max:this.maxyvalue,
            title: {
              display: true,
              text: this.Datavalue
            },
          
          },
        },
    
      
      }
      
  });
 
 
 
  }
update()
{
  

  this.chart.data.datasets.push([{
    label: 'Line Dataset',
    data: [10, 10, 10, 10],
    type: 'line',
    // this dataset is drawn on top
    order: 1
}]);

      this.chart.update();
}
  oncolorChange(event: any){
    this.selected1= event.target.value;
    this.updateplot();
    this.chart.data.datasets[0].borderColor=this.getRandomColor;
    
    this.chart.update();
  }

  onshapeChange(event: any){
    this.selected2= event.target.value;
     this.updateplot();
    for(var i=0;i<this.chart.data.datasets.length;i++){
      this.chart.data.datasets[i].pointStyle=this.shape[Math.floor(Math.random() * this.shape.length)];
    }
    this.chart.update();
  }

  generateLabels(chart: { data: any; getDatasetMeta: (arg0: number) => any; options: { elements: { arc: any; }; }; }) {
    var data = chart.data;
    if (data.labels.length && data.datasets.length) {
      return data.labels.map(function(label: string, i: string | number) {
        var meta = chart.getDatasetMeta(0);
        var ds = data.datasets[0];
      
          return {
          // And finally : 
          text: ds.data[i] + "% of the time " + label,
        
        };
      });
    }
    return [];
  }
   
}


