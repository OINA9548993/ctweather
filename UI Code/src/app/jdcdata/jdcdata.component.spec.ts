import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JdcdataComponent } from './jdcdata.component';

describe('JdcdataComponent', () => {
  let component: JdcdataComponent;
  let fixture: ComponentFixture<JdcdataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JdcdataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JdcdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
