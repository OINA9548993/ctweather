import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],

})
export class AppComponent {
  title = 'weather';
  currentDate = new Date();
  showmenu: boolean = false;

   URL: any;
  togglemenu(){
    this.showmenu = !this.showmenu;
  }


 
}