import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.less']
})
export class LogoutComponent implements OnInit {
  pathPrefix:any;
  constructor(private _global: GlobalService) { 
    this.pathPrefix=_global.pathPrefix;
  }

  ngOnInit() {
    $('.user-profile').remove();
    $('.navigation').remove();

   // $('.container-fluid').remove();
   //  window.location = "https://uat.websso.saint-gobain.com/cas/logout" ;
     sessionStorage.removeItem('SGID');
     sessionStorage.removeItem('Name');
     sessionStorage.clear();
     localStorage.clear();
     var cookies = document.cookie.split(";");
debugger;
     for (var i = 0; i < cookies.length; i++) {
         var cookie = cookies[i];
         var eqPos = cookie.indexOf("=");
         var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
         document.cookie = "CASTGCwebsso" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
         document.cookie = "F5_SSO" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
         document.cookie = "JSESSIONID" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
     }
 }
 Login(){
//  window.location.href = "http://localhost:4200/login" ;
  //window.location.href = "https://digital-team-825wb.certainteed.com/weather/login" ;

  window.location.href = "https://weather.certainteed.com/weather/login" ;;
 }

}
