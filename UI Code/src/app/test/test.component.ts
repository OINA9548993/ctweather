import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.less']
})
export class TestComponent implements OnInit {
 pageSize = 10;
 curPage = 1;
  coinsData:any;
  URL:any;
  data1: any;
  dtOptions: DataTables.Settings = {};
  persons: any;
  constructor(private http: HttpClient,private _global: GlobalService) {
    this.URL=_global.URL;
   }
   objectToHttpParams(params: any, data: any, currentPath: string) {
    Object.keys(data).forEach(key => {
        if (data[key] instanceof Object) {
          if (currentPath !== '' ) {
            this.objectToHttpParams(params, data[key], `${currentPath}[${key}]`);
          } else {
            this.objectToHttpParams(params, data[key], `${currentPath}${key}`);
          }
        } else {
            if (currentPath !== '' ) {
              params[`${currentPath}[${key}]`] = data[key];
            } else {
              params[`${currentPath}${key}`] = data[key];
            }
        }
    });
  }
  ngOnInit(): void {
debugger;

  
const that = this;

this.dtOptions = {
  pagingType: 'full_numbers',
  pageLength: 10,
  serverSide: true,
  processing: true,
  
  ajax: (dataTablesParameters: any, callback) => {
    const params = {};
     this.objectToHttpParams(params , dataTablesParameters , '');
        console.log('params', params);
    that.http
      .post(
        'http://localhost:54100/api/micdatabase/',
         dataTablesParameters
      ).subscribe((resp:any) => {
       // resp=JSON.stringify(resp)
       var jsonArray = JSON.parse(JSON.stringify(resp))
        this.persons=jsonArray.data ;
       // that.persons = resp.data;

        callback({
          recordsTotal: resp.recordsTotal,
          recordsFiltered: resp.recordsFiltered,
          data: []
        });
      });
  },
  "language": {
    "search": "",
    "searchPlaceholder": "Search..."
},
  columns: [{ data: 'ID' }, { data: 'Formulation' }, { data: 'Pigment' }]
};

  }
  async  renderTable(page = 1) {
    setTimeout(()=>{   
      this.http.get(this.URL+'api/micdatabase/').subscribe(data => {
  
        const response = data;
        const coins =  response
         this.coinsData = data;

         if (page == 1) {
          $("#prevButton")[0].style.visibility = "hidden";
        } else {
          $("#prevButton")[0].style.visibility = "visible";
        }
      
        if (page == this.numPages()) {
          $("#nextButton")[0].style.visibility = "hidden";
        } else {
          $("#nextButton")[0].style.visibility = "visible";
        }

        var cryptoCoin = "";
        this.coinsData.filter((row: any, index: number) => {
          let start = (this.curPage - 1) * this.pageSize;
          let end = this.curPage * this.pageSize;
          if (index >= start && index < end) 
          {
            return true;
          }
          else
          {
            return false;
          }
      
        }).forEach((coin: { [x: string]: string; }) => {
          cryptoCoin += "<tr>";
          cryptoCoin += "<td> "+coin["Formulation"] +" </td>";
          cryptoCoin += "<td> "+ coin["Pigment_Supplier"]+" </td>";
          "<tr>";
        
        });
        $("#data")[0].innerHTML = cryptoCoin;
      });

  
  }, 3000);


    // create html
   
 
  }

  async  getData() {
    this.http.get(this.URL+'api/micdatabase/').subscribe(data => {
  
      const response = data;
      const coins =  response
       this.coinsData = data;
    });
  //  const response = await this.http.get(this.URL+'api/micdatabase/');
    
   // this.coinsData = coins?.data.coins
  }
   previousPage() {
    if (this.curPage > 1) {
      this.curPage--;
      this.renderTable(this.curPage);
    }
  }
  
   nextPage() {
    if ((this.curPage * this.pageSize) < this.coinsData.length) {
      this.curPage++;
      this.renderTable(this.curPage);
    }
  }

  
   numPages() {
    return Math.ceil(this.coinsData.length / this.pageSize);
  }
  

}
