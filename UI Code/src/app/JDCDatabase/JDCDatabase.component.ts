import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import{CtweatherService } from '../ctweather.service'
import { GlobalService } from '../global.service';
import 'datatables.net-fixedheader';
import * as moment from 'moment';

@Component({
  selector: 'app-JDCDatabase',
  templateUrl: './JDCDatabase.component.html',
  styleUrls: ['./JDCDatabase.component.less']
})
export class JDCDatabaseComponent implements OnInit { 


data:any;
URL:any;
SGID:any;
@ViewChild(DataTableDirective, {static: false})
datatableElement: DataTableDirective | undefined;
constructor(private http: HttpClient,private _global: GlobalService,private route: Router){
   this.URL=_global.URL;
//get request from web api

}   


   ngOnInit(){
    this.http.get(this.URL+'api/Gettabledata/').subscribe(data => {
      this.data = data;
      var datatableInstance = $('#datatableexample').DataTable({
        paging: true,
        ordering: false,
        fixedHeader: true,
        searching: true,
        data: this.data ,
        columns: [
          
        { 'data': 'Test_ID' },
        { 'data': 'Test_name' },
        { 'data': 'test_type' },
        { 'data': 'test_location' },
        { 'data': 'Sample_code' },
        { 'data': 'test_location_id' },
        { 'data': 'RET' },
        { 'data': 'Color' },
        { 'data': 'Formulation' },
        { 'data': 'Sample_name' },
        { 'data': 'Test_Date' ,
        'render': function ( data, type, row ) {
          return moment(data).format("MM-DD-YYYY HH:mm");
      } },
        { 'data': 'TOH' },
        { 'data': 'Time' },
        { 'data': 'Time_metric' },
        { 'data': 'Gloss' },
        { 'data': 'L',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'a',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'b',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'DL',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'Da',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'Db',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'DE',
        'render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      }
      },
        { 'data': 'HBU','render': function ( data, type, row ) {
          return parseFloat(data).toFixed(2);
      } },
        { 'data': 'Comments' },
        { 'data': 'unique_ID' },
         
        ],
        
    });
    
    

    $('#datatableexample thead tr:eq(1) th').each(function () {
      
        var title = $('#datatableexample thead th').eq($(this).index()).text();
        if(title!="Delete")
        {
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    });
    
    datatableInstance.columns().every(function (index) {
      debugger;
   
     
        var dataTableColumn = this;
        
        var colname= this.header().textContent ;
        if(index==0 || index==1 || index==3 || index==5 || index==9)
        {
          var select = $('<select><option value=""></option></select>')
      .appendTo( $(dataTableColumn.header()).empty());

      dataTableColumn.data().unique().sort().each( function ( d, j ){
        select.append( '<option value="' + d + '">' + d + '</option>' );
      });
      $(this.header()).find('select').on('change', function () {
        dataTableColumn.search(this.value).draw();
    });
        }
        else{
        $(this.header()).find('input').on('keyup change', function () {
            dataTableColumn.search(this.value).draw();
        });
      }
    });

    });
    this.SGID=sessionStorage.getItem("SGID")?.toString();
    if(this.SGID==undefined)
    {
      this.route.navigate(['/login']);
    }
    else{
      $('.dateadded').on( 'change', function (ret :any) {
 
         var v = ret.target.value  // getting search input value
         
         $('#dataTables-example').DataTable().columns(3).search(v).draw();
       
     } );
   
  }
 
}



}
