import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JDCDatabaseComponent } from './JDCDatabase.component';

describe('JDCDatabaseComponent', () => {
  let component: JDCDatabaseComponent;
  let fixture: ComponentFixture<JDCDatabaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JDCDatabaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JDCDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
