import { TestBed } from '@angular/core/testing';

import { CtweatherService } from './ctweather.service';

describe('CtweatherService', () => {
  let service: CtweatherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CtweatherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
