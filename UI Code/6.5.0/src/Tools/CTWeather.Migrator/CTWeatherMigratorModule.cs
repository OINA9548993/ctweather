using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using CTWeather.EntityFramework;

namespace CTWeather.Migrator
{
    [DependsOn(typeof(CTWeatherDataModule))]
    public class CTWeatherMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<CTWeatherDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}