﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CTWeather.MultiTenancy.Dto;

namespace CTWeather.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
