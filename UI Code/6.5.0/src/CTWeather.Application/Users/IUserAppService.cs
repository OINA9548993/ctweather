using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CTWeather.Roles.Dto;
using CTWeather.Users.Dto;

namespace CTWeather.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}