﻿using System.Threading.Tasks;
using Abp.Application.Services;
using CTWeather.Configuration.Dto;

namespace CTWeather.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}