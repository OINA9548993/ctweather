﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using CTWeather.Configuration.Dto;

namespace CTWeather.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : CTWeatherAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
