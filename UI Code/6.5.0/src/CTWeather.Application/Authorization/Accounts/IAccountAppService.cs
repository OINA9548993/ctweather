﻿using System.Threading.Tasks;
using Abp.Application.Services;
using CTWeather.Authorization.Accounts.Dto;

namespace CTWeather.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
