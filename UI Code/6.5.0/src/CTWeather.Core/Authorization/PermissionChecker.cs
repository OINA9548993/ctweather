﻿using Abp.Authorization;
using CTWeather.Authorization.Roles;
using CTWeather.Authorization.Users;

namespace CTWeather.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
