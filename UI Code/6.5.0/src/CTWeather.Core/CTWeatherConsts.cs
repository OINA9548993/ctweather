﻿namespace CTWeather
{
    public class CTWeatherConsts
    {
        public const string LocalizationSourceName = "CTWeather";

        public const bool MultiTenancyEnabled = true;
        
        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "a9641aec6a8d49c0a94ceca1063c36b5";
    }
}