using Abp.AutoMapper;
using CTWeather.Sessions.Dto;

namespace CTWeather.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}