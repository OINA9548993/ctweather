﻿using Abp.Web.Mvc.Views;

namespace CTWeather.Web.Views
{
    public abstract class CTWeatherWebViewPageBase : CTWeatherWebViewPageBase<dynamic>
    {

    }

    public abstract class CTWeatherWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected CTWeatherWebViewPageBase()
        {
            LocalizationSourceName = CTWeatherConsts.LocalizationSourceName;
        }
    }
}