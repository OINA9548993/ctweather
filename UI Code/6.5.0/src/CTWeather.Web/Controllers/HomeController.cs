﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace CTWeather.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : CTWeatherControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}