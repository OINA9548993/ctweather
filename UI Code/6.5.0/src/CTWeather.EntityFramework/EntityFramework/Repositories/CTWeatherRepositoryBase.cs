﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace CTWeather.EntityFramework.Repositories
{
    public abstract class CTWeatherRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<CTWeatherDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected CTWeatherRepositoryBase(IDbContextProvider<CTWeatherDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class CTWeatherRepositoryBase<TEntity> : CTWeatherRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected CTWeatherRepositoryBase(IDbContextProvider<CTWeatherDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
