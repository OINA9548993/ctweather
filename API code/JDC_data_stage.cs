//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CTWeather_API
{
    using System;
    using System.Collections.Generic;
    
    public partial class JDC_data_stage
    {
        public string Filepath { get; set; }
        public string filename { get; set; }
        public string sheet_name { get; set; }
        public string Test_ID { get; set; }
        public string Test_name { get; set; }
        public string test_type { get; set; }
        public string test_location { get; set; }
        public string Sample_code { get; set; }
        public string test_location_id { get; set; }
        public string RET { get; set; }
        public string Color { get; set; }
        public string Formulation { get; set; }
        public string Sample_name { get; set; }
        public Nullable<System.DateTime> Test_Date { get; set; }
        public Nullable<double> TOH { get; set; }
        public Nullable<double> Time { get; set; }
        public string Time_metric { get; set; }
        public Nullable<double> Gloss { get; set; }
        public Nullable<double> L { get; set; }
        public Nullable<double> a { get; set; }
        public Nullable<double> b { get; set; }
        public Nullable<double> DL { get; set; }
        public Nullable<double> Da { get; set; }
        public Nullable<double> Db { get; set; }
        public Nullable<double> DE { get; set; }
        public Nullable<double> HBU { get; set; }
        public string Comments { get; set; }
        public string unique_ID { get; set; }
        public int ID { get; set; }
        public Nullable<System.DateTime> importedDate { get; set; }
        public Nullable<bool> isUpdated { get; set; }
    }
}
