﻿using ExcelDataReader;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CTWeather_API.Controllers
{
 
    public class LoginController : AbstractController
    {
        // GET: Login
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/sso1")]
        public string Sso(string ticket)
        {
            var token = string.Empty;
            var SGID = string.Empty;
            if (!string.IsNullOrWhiteSpace(ticket))
            {
                // Retrieve SGId from CAS server
                var sgId = RetrieveSGIdFromTicket(ticket, Request.RequestUri.GetLeftPart(UriPartial.Path)); //"K6698487";
                if (!string.IsNullOrWhiteSpace(sgId))
                {
                    // Check if user exists in database
                    var user = Repository.FindBy<ct_siding_user>(u => u.SGI == sgId)

                         .FirstOrDefault();

                    token = user.Token1.value;
                    SGID = user.SGI;
                    return user.SGI;
                }
            }
            return SGID;
            //return Redirect(Settings.Default.WebApplicationUrl + "forbidden");
        }

        private string RetrieveSGIdFromTicket(string ticket, string service)
        {
            using (var webClient = new WebClient())
            {
                 var validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
              //  var validateUrl = "https://websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
                var stream = webClient.OpenRead(validateUrl);
                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    var response = reader.ReadToEnd();
                    var ssoResult = response.Split(new[] { '\n' }, StringSplitOptions.None);
                    if (ssoResult[0].Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return ssoResult[1];
                    }
                }
            }
            return null;
        }


        //{
        //    var token = string.Empty;
        //    var SGID = string.Empty;

        //    return SGID;
        //    //return Redirect(Settings.Default.WebApplicationUrl + "forbidden");
        //}

        public object Excel([FromBody] object customer)
        {
            SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();


            string[] arr = ((IEnumerable)customer).Cast<object>()
                             .Select(x => x.ToString())
                             .ToArray();

            var customers = objEntity.Set<pp_data_stage>();
            customers.Add(new pp_data_stage { ID = double.Parse(arr[0]), Pigment = arr[4] });

            objEntity.SaveChanges();

            return customer;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/CheckExcel")]
        public string CheckExcel()
        {
            string message = "";
            int i, j, rows, k = 1;


            rows = 6;
            for (i = 1; i <= rows; i++)
            {
                for (j = 1; j <= i; j++)
                    Console.WriteLine("{0} ", k++);
                Console.Write("\n");
            }
            return message;
        }

        private bool ReadExcelToTable(string path, string[] columndata)
        {
            bool condition = true;
            //Connection String  
            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"; // Extra blank space cannot appear in Office 2007 and the last version. And we need to pay attention on semicolon.  
                                                                                                                                             //   string connstring = Provider = Microsoft.JET.OLEDB .4 .0;
                                                                                                                                             //Data Source = " + path + ";
                                                                                                                                             //    Extended Properties = " 'Excel 8.0;HDR=NO;IMEX=1';"; //This connection string is appropriate for Office 2007 and the older version. We can select the most suitable connection string according to Office version or our program.  
            using (OleDbConnection conn = new OleDbConnection(connstring))
            {
                conn.Open();
                System.Data.DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" }); //Get All Sheets Name  
                string firstSheetName = sheetsName.Rows[0][2].ToString(); //Get the First Sheet Name  
                string query = "select * from  [Test List$]";
                OleDbConnection objConn;
                OleDbDataAdapter oleDA;
                System.Data.DataTable dt = new System.Data.DataTable();
                objConn = new OleDbConnection(connstring);
                objConn.Open();
                oleDA = new OleDbDataAdapter(query, objConn);
                oleDA.Fill(dt);
                objConn.Close();
                oleDA.Dispose();
                objConn.Dispose();

                for (int i = 2, j = 0; i < columndata.Length; i++, j++)
                {
                    if (columndata[i].ToString().Replace("null", "") == dt.Rows[0][j].ToString().Replace("*", ""))
                    {
                        condition = true;
                    }
                    else
                    {
                        condition = false;
                        return condition;
                    }
                }


            }
            return condition;
        }

        private bool ReadExcelToTableMain(string path, string[] columndata)
        {
            bool condition = true;
            //Connection String  
            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"; // Extra blank space cannot appear in Office 2007 and the last version. And we need to pay attention on semicolon.  
                                                                                                                                             //   string connstring = Provider = Microsoft.JET.OLEDB .4 .0;
                                                                                                                                             //Data Source = " + path + ";
                                                                                                                                             //    Extended Properties = " 'Excel 8.0;HDR=NO;IMEX=1';"; //This connection string is appropriate for Office 2007 and the older version. We can select the most suitable connection string according to Office version or our program.  
            using (OleDbConnection conn = new OleDbConnection(connstring))
            {
                conn.Open();
                System.Data.DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" }); //Get All Sheets Name  
                string firstSheetName = sheetsName.Rows[0][2].ToString(); //Get the First Sheet Name  
                string query = "select * from  [Master Test List$]";
                OleDbConnection objConn;
                OleDbDataAdapter oleDA;
                System.Data.DataTable dt = new System.Data.DataTable();
                objConn = new OleDbConnection(connstring);
                objConn.Open();
                oleDA = new OleDbDataAdapter(query, objConn);
                oleDA.Fill(dt);
                objConn.Close();
                oleDA.Dispose();
                objConn.Dispose();
                columndata = columndata.Distinct().ToArray();
                for (int i = 0, j = 0; i < columndata.Length; i++, j++)
                {
                    string row = Regex.Replace(dt.Rows[0][j].ToString().Replace("*", ""), @"\s+", "");
                    row = row.Replace("(%)", "");
                    if (columndata[i].ToString().Replace("null", "") == row)
                    {
                        condition = true;
                    }
                    else
                    {
                        condition = false;
                        return condition;
                    }
                }


            }
            return condition;
        }


    

        [System.Web.Http.HttpPost]

        [System.Web.Http.Route("api/Excel")]
        public IHttpActionResult ExcelUpload(dynamic data)
        {
            
            try
            {
                string message = "";
                string fileName = "MICExcel.xlsx";
                string path = "Files";
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path, fileName);
                string[] alldata = ((IEnumerable)data).Cast<object>()
                                 .Select(x => x.ToString())

                                .ToArray();
                string[] rowdata = alldata[1].Split(new string[] { "]," }, StringSplitOptions.None);
                string row = rowdata[2];
                row = Regex.Replace(row, @"\s+", "");
                row = row.Replace("\"", "");
                row = row.Replace("[", "");
                row = row.Replace("]", "");
                row = row.Replace("*", "");
                //if (row.Contains("Returntolist") == true)
                //{
                //    int start = row.LastIndexOf("DE") + "DE".Length;
                //    int end = row.IndexOf("Returntolist", start);
                //    row = row.Remove(start, end - start);
                //    row = row.Replace("Returntolist", "");
                //    start = 0;
                //    end = 0;
                //}

                int start = row.LastIndexOf("DE") + "DE".Length;
                if (start >= 0 && start < row.Length)
                {
                    row = row.Substring(0, start + 1);
                    row = row.Remove(row.Length - 1);
                }
                string[] columndata = row.Split(',');
                string column = "";
                for (int i = 0; i < columndata.Length; i++)
                {
                    if (columndata[i] == "null")
                    {
                        column += columndata[i] + i + " varchar(250),";
                    }
                    else if (column.Contains(columndata[i]) == true)
                    {
                        column += columndata[i] + i + "  float NULL,";
                    }
                    else
                    {
                        column += columndata[i] + " float NULL,";
                    }
                }
                column += "TableID int,";
                column = column.Replace("D17", "DE17");
                column = column.Remove(column.Length - 1);
                alldata[0] = alldata[0].Replace(", McP", " McP");


                bool condition = true;//ReadExcelToTable(path, columndata);
                if (condition == true)
                {

                    SqlConnection conn = new SqlConnection("Data source=H90SV30600825WB; Database=SGRNADataScience;User Id=SGCSDevelopmentSQLAccount;Password=!Flick3R!*@C4");

                  //  SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                    SqlCommand cmd = new SqlCommand("create table [" + alldata[0].ToString() + "]([id] [int] IDENTITY(1,1) NOT NULL," + column + ");", conn);
                    conn.Open();
                    try { 
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("Table Created Successfully...");
                    Console.WriteLine("Table Created Successfully...");
                    column = column.Insert(0, "[");
                    column = column.Replace("float NULL", "");
                    column = column.Replace("varchar(250)", "");
                    column = column.Replace("int", "");
                    column = column.Replace(",", "],[");
                    column = column.Insert(column.Length - 1, "]");

                    string query1 = "INSERT INTO [" + alldata[0].ToString() + "]" + "(" + column + ") VALUES (";
                    rowdata = rowdata.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    int TableID = 0;
                    for (int j = 3; j < rowdata.Length; j++)
                    {

                        //    if (rowdata[j].Contains("END") == true)
                        //    {
                        //        return "";
                        //    }
                        //else
                        //{


                        string query = "";
                        string row1 = rowdata[j];
                        //  row1 = Regex.Replace(row1, @"\s+", "");
                        row1 = row1.Replace("\"", "");
                        row1 = row1.Replace("[", "");
                        row1 = row1.Replace("]", "");
                        row1 = row1.Replace("*", "");

                        if (row1.Contains("Hours") == true)
                        {
                            TableID++;
                        }
                        if (row1 != "")
                        {


                            string[] columndata1 = row1.Split(',');
                            float number;
                            for (int k = 0; k < columndata1.Length; k++)

                            {
                                if (columndata1.Length > columndata.Length)
                                {
                                }
                                else
                                {
                                    if (columndata1[k] == "null")
                                    {
                                        query += "NULL" + ",";
                                    }
                                    else if (float.TryParse(columndata1[k], out number))
                                    {
                                        query += "'" + number + "',";
                                    }
                                    else
                                    {

                                        query += "NULL" + ",";
                                    }
                                }
                            }
                            if (columndata1.Length < columndata.Length)
                            {
                                for (int n = 0; n < columndata.Length - columndata1.Length; n++)
                                {
                                    query += "NULL" + ",";
                                }
                            }
                            if (query != "")
                            {
                                query += TableID + ",";
                                query = query.Remove(query.Length - 1);
                                query += ")";

                                cmd = new SqlCommand(query1 + query, conn);

                                cmd.ExecuteNonQuery();
                                query = "";

                                cmd = new SqlCommand("Delete from [" + alldata[0].ToString() + "] where Hours is null", conn);

                                cmd.ExecuteNonQuery();
                                cmd = new SqlCommand("Delete from [" + alldata[0].ToString() + "] where L3 is null", conn);

                                cmd.ExecuteNonQuery();
                            }
                        }
                        //  }
                    }

                    conn.Close();
                    alldata = null;

                    rowdata = null;
                    row = "";



                    columndata = null;
                    column = "";
                    query1 = "";
                    message = "success";
                }
                    finally
                    {
                        conn.Dispose(); // And this will also close the connection
                    }
                }
                else
                {
                    try
                    {
                        objEntity.DeleteTables();
                    }
                    catch
                    {

                    }

                    message = "Please upload valid excel sheet";

                }


                return Ok("success");

            }
            catch (Exception e)
            {
                return Ok(e.InnerException);
            }
        }


        



        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/ExcelData")]
        public IHttpActionResult ExcelData(dynamic data)
        {
            try
            {
               
                string message = "";
                string fileName = "MICExcel.xlsx";
                string path = "Files";
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path, fileName);
                string[] alldata = ((IEnumerable)data).Cast<object>()
                                 .Select(x => x.ToString())

                                .ToArray();

                string[] rowdata = alldata[1].Split(new string[] { "]," }, StringSplitOptions.None); ;

                string row = rowdata[0];
                row = Regex.Replace(row, @"\s+", "");
                row = row.Replace("\"", "");
                row = row.Replace("[", "");
                row = row.Replace("]", "");
                row = row.Replace("*", "");
                row = row.Replace("(%)", "");

                string[] columndata = row.Split(',');

                bool condition = ReadExcelToTableMain(path, columndata);

                if (condition == true)
                {
                    string Hidden = alldata[2];
                    List<String> list = new List<String>();

                    List<String> index = new List<String>();
                    //   Hidden = Regex.Replace(Hidden, @"\s+", "_");
                    Hidden = Hidden.Replace("\"", "");
                    Hidden = Hidden.Replace("[", "");
                    Hidden = Hidden.Replace("]", "");
                    Hidden = Hidden.Replace("*", "");
                    Hidden = Hidden.Replace("(%)", "");
                    Hidden = Hidden.Replace(", McP", " McP");
                    string[] Hiddendata = Hidden.Split(',');
                    SqlConnection conn = new SqlConnection("Data source = H90SV30600825WB; Database = SGRNADataScience; User Id = SGCSDevelopmentSQLAccount; Password = !Flick3R!*@C4");
                  //  SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                    SqlCommand cmd = new SqlCommand("create table [SheetNames]( [id] [int] IDENTITY(1,1) NOT NULL,[Name][nvarchar](255),[SheetNumber] [nvarchar](255) );", conn);
                    conn.Open();
                    try { 
                    cmd.ExecuteNonQuery();
                    for (int g = 3; g < Hiddendata.Length; g++)
                    {

                        var words = Hiddendata[g].Split(' ');
                        if (words[1].Contains("McP") == false)
                        {
                            var white = words[2];
                            cmd = new SqlCommand("INSERT INTO [SheetNames](Name,SheetNumber) VALUES ('" + Hiddendata[g].TrimStart().TrimEnd() + "','" + white + "')", conn);

                            cmd.ExecuteNonQuery();
                        }

                    }

                    string column = "";
                     
                        for (int i = 0; i < columndata.Length; i++)
                    {


                        column += columndata[i].TrimStart().TrimEnd() + " varchar(250),";

                    }

                    column = column.Remove(column.Length - 1);


                    //  conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                    cmd = new SqlCommand("create table [" + alldata[0].ToString() + "]([idnum] [int] IDENTITY(1,1) NOT NULL," + column + ");", conn);

                    cmd.ExecuteNonQuery();
                    Console.WriteLine("Table Created Successfully...");
                    column = column.Insert(0, "[");
                    column = column.Replace("varchar(250)", "");
                    column = column.Replace(",", "],[");
                    column = column.Insert(column.Length - 1, "]");

                    string query1 = "INSERT INTO [" + alldata[0].ToString() + "]" + "(" + column + ") VALUES (";
                    // int TableID=0;
                    for (int j = 1; j < rowdata.Length; j++)
                    {

                        string query = "";
                        string row1 = rowdata[j];
                        //   row1 = Regex.Replace(row1, @"\s+", "");
                      //  row1 = row1.Replace("\"", "");
                        row1 = row1.Replace("[", "");
                        row1 = row1.Replace("]", "");
                        row1 = row1.Replace("*", "");
                        row1 = row1.Replace("null", "null\"");
                            string[] columndata1 = row1.Split(new string[] { "\"," }, StringSplitOptions.None);
                        for (int k = 0; k < columndata1.Length; k++)

                        {

                                columndata1[k] = columndata1[k].Replace("\"", "");
                                if (columndata1[k].TrimStart().TrimEnd() == "null")
                            {
                                query += "NULL" + ",";
                            }
                            else if (columndata1[k].TrimStart().TrimEnd() == "NA")
                            {
                                query += "NULL" + ",";
                            }
                            else
                            {

                                query += "'" + columndata1[k].TrimStart().TrimEnd() + "',";
                            }

                        }
                        if (columndata1.Length < columndata.Length)
                        {
                            for (int n = 0; n < columndata.Length - columndata1.Length; n++)
                            {
                                query += "NULL" + ",";
                            }
                        }
                        // query += TableID + ",";
                        query = query.Remove(query.Length - 1);
                        query += ")";

                        cmd = new SqlCommand(query1 + query, conn);

                        cmd.ExecuteNonQuery();
                        query = "";

                    }

                    conn.Close();
                    alldata = null;

                    rowdata = null;
                    row = "";



                    columndata = null;
                    column = "";
                    query1 = "";
                    message = "Sucess";
                }
                    catch (Exception e)
                    {
                        return Ok(e.InnerException);
                    }
                    finally
                    {
                        conn.Dispose(); // And this will also close the connection
                    }
                }
                else
                {
                    try
                    {
                        objEntity.DeleteTables();
                    }
                    catch
                    {

                    }

                    message = "Please upload valid excel sheet";

                }
                return Ok(message);
                
            }
            catch(Exception e)
            {
                return Ok(e.InnerException);
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/ImpportfromExcel")]
        public string ImpportfromExcel()
        {
            string message = "";
            try
            {
                // int a = 0;
                int a = objEntity.ImportfromExcel();
                a = objEntity.DeleteTables();

                return "Success";

            }
            catch (Exception e)
            {
                try
                {
                    objEntity.DeleteTables();
                }
                catch
                {

                }
                string m = e.Message;
                return e.InnerException.ToString();
            }
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/updateData")]
        public string updateData()
        {

            try
            {

                // int a = 0;
                int a = objEntity.updateData();
                return "Success";

            }
            catch (Exception e)
            {
                try
                {
                    objEntity.DeleteTables();
                }
                catch
                {

                }
                string m = e.Message;
                return e.InnerException.ToString();
            }
        }


     
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/InsertTestCondition")]
        public string InsertTestCondition(object Testdata)
        {
            string m = "";
            try
            {
                using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
                {
                    db.QUV_test_param.ToList().ForEach(c =>
                    {
                        c.IsDelete = true;
                    });
                    db.SaveChanges();
                }

                string[] alldata = ((IEnumerable)Testdata).Cast<object>()
                            .Select(x => x.ToString())

                           .ToArray();
                for (int i = 0; i < alldata.Length; i++)
                {
                    string row = alldata[i];
                    row = row.Replace("[", "");
                    row = row.Replace("]", "");
                    row = row.Replace("\"", "");
                    if (row != "")
                    {
                       // row = Regex.Replace(row, @"\s+", "");
                        string[] columndata = row.Split(',');
                        QUV_test_param QUV = new QUV_test_param();
                        QUV.QUV_id = columndata[0].ToString().TrimStart();
                   
                        QUV.uv_irradiance = columndata[1].ToString().TrimStart() != "" ? double.Parse(columndata[1].ToString().TrimStart()) : 0;
                        QUV.uv_cycle_time = columndata[2].ToString().TrimStart() != "" ? double.Parse(columndata[2].ToString().TrimStart()) : 0;
                        QUV.uv_cycle_temp = columndata[3].ToString().TrimStart() != "" ? double.Parse(columndata[3].ToString().TrimStart()) : 0;
                        QUV.condensation_cycle_temp = columndata[5].ToString().TrimStart() != "" ? double.Parse(columndata[5].ToString().TrimStart()) : 0;
                        QUV.condensation_cycle_time = columndata[4].ToString().TrimStart() != "" ? double.Parse(columndata[4].ToString().TrimStart()) : 0;
                        QUV.comments = columndata[6].ToString().TrimStart();
                        QUV.IsDelete = false;

                        objEntity.QUV_test_param.Add(QUV);


                        int output1 = objEntity.SaveChanges();
                    }
                }
                    return "Success";
            }
            catch (Exception e)
            {

                 m = e.Message;
                return e.InnerException.ToString();
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/DeleteCondition/{id}")]
        public string DeleteCondition(int id)
        {

            try
            {
                using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
                {
                    db.QUV_test_param.Where(a=> a.ID==id).ToList().ForEach(c =>
                    {
                        c.IsDelete = true;
                    });
                    db.SaveChanges();
                }

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/DeleteMIC")]
        public string DeleteMIC(object id)
        {
            string[] alldata = ((IEnumerable)id).Cast<object>()
                           .Select(x => x.ToString()).ToArray();
            var tmp = alldata[0];
            var tmp1 = alldata[1];
            try
            {
                using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
                {
                   
                    var unique_ID = (from c in db.pp_data_unique
                                     where (c.Formulation == tmp)
                                     where (c.Pigment==tmp1)
                                     select c);
                    db.pp_data_unique.RemoveRange(unique_ID);
                    db.SaveChanges();
                }

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }
 
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Getcolum/{table}")]
        public IEnumerable<object> Getcolum(string table)
        {

            try
            {
                if (table == "MIC")
                {
                    return from t in typeof(pp_data_stage).GetProperties()
                               //   orderby t.Name
                           select t.Name;
                }
               else if  (table == "xtest")
                {
                    return from t in typeof(Xe_test_param).GetProperties()
                               //   orderby t.Name
                           select t.Name;
                }
                else
                {
                    return from t in typeof(pp_data_stage).GetProperties()
                               // orderby t.Name
                           select t.Name;
                }


            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetExcelImportedData/{table}")]
        public IEnumerable<object> GetExcelImportedData(string table)
        {

            try
            {
                if (table == "MIC")
                {
                    var data= objEntity.GetMICData().ToArray();
                    return data;
                }
                else
                {
                    return objEntity.GetJDCData().ToArray();
                }


            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}