﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Http;
using CTWeather_API;

namespace CTWeather_API.Controllers
{
    public class AbstractController : ApiController
    {
       // GET: Abstract
        protected IRepository Repository
        {
            get
            {
                if (HttpContext.Current.Items["Repository"] == null)
                    HttpContext.Current.Items["Repository"] = new Repository(new SGRNADataScienceEntities());
                return (IRepository)HttpContext.Current.Items["Repository"];
            }
        }

       
    }
}