﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;


namespace CTWeather_API.Controllers
{
    public class MICController : AbstractController
    {
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/micdatabase1")]
        public IEnumerable<pp_data_unique> GetEmaployee()
        {
            try
            {
                //int i = 0;
              //  pagenumber = pagenumber - 1;
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.pp_data_unique.ToArray();
                return data;
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/micdatabase")]
        public object GetEmployeeList(DataTableAjaxPostModel parameters)
        {
           
            int totalRecord = 0;
            int filterRecord = 0;
            var sortColumn="";
            var sortColumnDirection = false;
           
                var draw = parameters.draw;
                if (parameters.order != null)
                {
                    // in this example we just default sort on the 1st column
                    sortColumn = parameters.columns[parameters.order[0].column].data;
                    sortColumnDirection = parameters.order[0].dir.ToLower() == "asc";
                }
                //var sortColumn = HttpContext.Current.Request.Form["columns[" + HttpContext.Current.Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
               // var sortColumnDirection = HttpContext.Current.Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = (parameters.search != null) ? parameters.search.value : null;
                int pageSize = parameters.length;
                int skip = parameters.length;
            
            var data = objEntity.Set<pp_data_unique>().AsQueryable();
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            if (!string.IsNullOrEmpty(searchValue))
           {
                data = data.Where(x => x.Formulation.ToLower().Contains(searchValue.ToLower()) || x.Pigment.ToLower().Contains(searchValue.ToLower()));
            }

            for (int i=0;i< parameters.columns.Count;i++)
            {
                if (parameters.columns[i].search.value != "")
                {
                    var sv = parameters.columns[i].search.value.ToString();
                    if (i == 2)
                    {
                        data = data.Where(x => x.Pigment.ToLower().Contains(sv));
                    }
                    else if (i == 0)
                    {
                        data = data.Where(x => x.ID.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 1)
                    {
                        data = data.Where(x => x.Formulation.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 3)
                    {
                        data = data.Where(x => x.Date.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 4)
                    {
                        data = data.Where(x => x.duplicate.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 5)
                    {
                        data = data.Where(x => x.Hours.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 6)
                    {
                        data = data.Where(x => x.Lstar.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 7)
                    {
                        data = data.Where(x => x.astar.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 8)
                    {
                        data = data.Where(x => x.bstar.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 9)
                    {
                        data = data.Where(x => x.dL.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 10)
                    {
                        data = data.Where(x => x.da.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 11)
                    {
                        data = data.Where(x => x.db.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 12)
                    {
                        data = data.Where(x => x.dE.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 13)
                    {
                        data = data.Where(x => x.Light_Stabilizer_Type.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 14)
                    {
                        data = data.Where(x => x.Target_Light_Stabilizer_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 15)
                    {
                        data = data.Where(x => x.Actual_Light_Stabilizer_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 16)
                    {
                        data = data.Where(x => x.Antioxidant.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 17)
                    {
                        data = data.Where(x => x.Antioxidant_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 18)
                    {
                        data = data.Where(x => x.Filler_Type.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 19)
                    {
                        data = data.Where(x => x.Filler_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 20)
                    {
                        data = data.Where(x => x.Pigment_Type.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 21)
                    {
                        data = data.Where(x => x.Pigment_Supplier.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 22)
                    {
                        data = data.Where(x => x.Pigment_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 23)
                    {
                        data = data.Where(x => x.Impact_Modifier_Type.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 24)
                    {
                        data = data.Where(x => x.Impact_Modifier_Content.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 25)
                    {
                        data = data.Where(x => x.Processing_Location.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 26)
                    {
                        data = data.Where(x => x.Mold.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 27)
                    {
                        data = data.Where(x => x.Resin_Grade.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 28)
                    {
                        data = data.Where(x => x.unique_ID.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 29)
                    {
                        data = data.Where(x => x.Compound.ToString().ToLower().Contains(sv));

                    }
                    else if (i == 30)
                    {
                        data = data.Where(x => x.Compound_Lot.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 31)
                    {
                        data = data.Where(x => x.Press.ToString().ToLower().Contains(sv));
                    }
                    else if (i == 32)
                    {
                        data = data.Where(x => x.Notes.ToString().ToLower().Contains(sv));
                    }
                }
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
        //    if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection.ToString())) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.OrderBy(p => sortColumn + " " + sortColumnDirection).Skip(skip).Take(pageSize).ToList();
           // var result = new JsonResult();
            //result.Data = new
            //{
            //    draw = 0,
            //    recordsTotal = totalRecord,
            //    recordsFiltered = filterRecord,
            //    data = empList
            //};
            var returnObj = new { draw = 0, recordsTotal = totalRecord, recordsFiltered = filterRecord, data = empList };
            return Json(returnObj);
        }
    

   
      
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetMICSummary")]
        public List<object> GetJDCSummary()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data= objEntity.pp_data_unique.GroupBy(c => new { c.ID, c.Pigment, c.Formulation }).Select(gcs => new
                {
                    gcs.Key.ID,
                    gcs.Key.Formulation,
                    gcs.Key.Pigment,
                  
                    max_hours = (from t2 in gcs select t2.Hours).Max(),
                    //  Duplicates = (from t2 in gcs select t2.duplicate).Distinct(),
                    Date = (from t2 in gcs select t2.Date).Max(),
                    Light_Stabilizer_Type = (from t2 in gcs select t2.Light_Stabilizer_Type).Distinct(),
                    Filler_Type = (from t2 in gcs select t2.Filler_Type).Distinct(),
                    Pigment_Supplier = (from t2 in gcs select t2.Pigment_Supplier).Distinct(),
                    Processing_Location = (from t2 in gcs select t2.Processing_Location).Distinct(),
                    Resin_Grade = (from t2 in gcs select t2.Resin_Grade).Distinct(),


                }).ToList<object>();

                  return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetMICControlData")]
        public List<object> GetMICControlData()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                string searchStr = "Control";
                return objEntity.pp_data_unique.Where(X => X.Formulation.Contains(searchStr)).GroupBy(c => new { c.ID, c.Pigment, c.Formulation }).Select(gcs => new 
                {
                   
                    gcs.Key.Formulation,
                 


                }).Distinct().ToList<object>();

                //   return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetIDfromControlData")]
        public List<object> GetIDfromControlData(string formulation,string Pigments)
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
              
                return objEntity.pp_data_unique.Where(X => X.Formulation.Contains(formulation) && Pigments.Contains(X.Pigment)).GroupBy(c => new { c.ID, c.Pigment, c.Formulation }).Select(gcs => new
                {

                    gcs.Key.ID,



                }).ToList<object>();

                //   return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetMICColumnName")]
        public List<string> GetMICColumnName()
        {
            try
            {
                var exceptionList = new List<string> {  "duplicate" ,"Hours"
      ,"Lstar"
      ,"astar"
      ,"bstar"
      ,"dL"
      ,"da"
      ,"db"
      ,"dE","UniqueID" };
                var names = typeof(pp_data_unique).GetProperties().Where(property => !exceptionList.Contains(property.Name))
                         .Select(property => property.Name)
                         .ToList();
                return names;
            }
            catch (Exception)
            {
                throw;
            }
        }





        }
}