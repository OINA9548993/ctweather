﻿using ExcelDataReader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CTWeather_API.Controllers
{
    public class JDCController : AbstractController
    {
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/GetJDCGraphdata/{Ids}")]
        public List<object> GetGraphdata(string Ids)
        {
            try
            {
          
                return objEntity.JDC_data_unique.Where(pp => pp.Time != 0 && Ids.Contains(pp.test_location_id)).Select(gcs => new
                {
                    gcs.DE,
                    gcs.Da,
                    gcs.Db,
                    gcs.DL,
                    gcs.test_location_id,
                    gcs.Formulation,
                    gcs.Color,
                    gcs.filename,
                    gcs.Test_Date,
                    gcs.Test_ID,
                    gcs.HBU,
                    gcs.Time_metric,
                    gcs.Time,
                    gcs.test_location,
                    gcs.Test_name,
                    gcs.test_type,
                    gcs.Sample_code,
                    gcs.RET,
                    gcs.Sample_name,
                    gcs.Comments,
                    gcs.Gloss,
                   
                    Hours = gcs.Time_metric == "Month" ? gcs.Time*730:gcs.Time,

                }).OrderBy(pp => pp.Hours).ToList<object>();

            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/GetJDCExceldata")]
        public List<object> GetJDCExceldata(object Ids)
        {
            try
            {
                string[] alldata = ((IEnumerable)Ids).Cast<object>()
                         .Select(x => x.ToString()).ToArray();

                return objEntity.JDC_data_unique.Where(pp => pp.Time != 0 && alldata.Any(name => name==pp.test_location_id.ToString())).Select(gcs => new
                {
                    
                    gcs.test_location_id,
                    gcs.Formulation,
                    gcs.Color,
                    gcs.filename,
                    gcs.DE,
                    gcs.Da,
                    gcs.Db,
                    gcs.DL,
                    gcs.Test_Date,
                    gcs.Test_ID,
                    gcs.HBU,
                    gcs.Time_metric,
                    gcs.Time,
                    gcs.test_location,
                    gcs.Test_name,
                    gcs.test_type,
                    gcs.Sample_code,
                    gcs.RET,
                    gcs.Sample_name,
                    gcs.Comments,
                    gcs.Gloss,

                    Hours = gcs.Time_metric == "Month" ? gcs.Time * 730 : gcs.Time,

                }).OrderBy(pp => pp.Hours).ToList<object>();

            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetGraphdata/{Ids}")]
        public List<object> GetMICGraphdata(string Ids)
        {
            Ids = Ids.Replace(',', '.');
            try
            {
                return objEntity.pp_data_unique.Where(pp => pp.duplicate == 1 && Ids==pp.ID.ToString()).OrderBy(pp => pp.Hours).

                    ToList<object>();


            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/GetExceldata")]
        public List<object> GetExceldata(object Ids)
        {
            string[] alldata = ((IEnumerable)Ids).Cast<object>()
                         .Select(x => x.ToString()).ToArray();
           // Ids = Ids.Replace(',', '.');
            try
            {
                //return objEntity.pp_data_unique.Where(pp => pp.duplicate == 1 && Ids.Contains(pp.ID.ToString())).OrderBy(pp => pp.Hours).

                //    ToList<object>();
                var meds = (from m in objEntity.pp_data_unique
                            where alldata.Any(name => name==m.ID.ToString())
                            select m).OrderBy(pp => pp.Hours).ToList<object>();

                return meds;

            }
            catch (Exception)
            {
                throw;
            }
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetJDCSummary")]
        public List<object> GetJDCSummary()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                return objEntity.JDC_data_unique.GroupBy(c => new { c.Time_metric, c.test_location, c.Test_ID, c.Sample_code }).Select(gcs => new
                {
                    gcs.Key.Test_ID,
                    gcs.Key.test_location,
                    gcs.Key.Sample_code,
                    gcs.Key.Time_metric,
                    max_time = (from t2 in gcs select t2.Time).Max(),
                    test_type = (from t2 in gcs select t2.test_type).Max(),
                    test_location_id = (from t2 in gcs select t2.test_location_id).Max(),
                    color = (from t2 in gcs select t2.Color).Max(),
                    Formulation = (from t2 in gcs select t2.Formulation).Max(),
                    Sample_name = (from t2 in gcs select t2.Sample_name).Max(),
                    HBU = (from t2 in gcs select t2.HBU).Max(),
                    Gloss = (from t2 in gcs select t2.Gloss).Max(),
                    Comments = (from t2 in gcs select t2.Comments).Max(),


                }).ToList<object>();

                //   return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetJdcXenontestcondition")]
        public IEnumerable<Xe_test_param> GetJdcXenontestcondition()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.Xe_test_param.Where(pp => pp.IsDelete == false).ToArray();
                return data;
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetJdctestcondition")]
        public IEnumerable<QUV_test_param> GetJdctestcondition()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.QUV_test_param.Where(a => a.IsDelete == false).ToArray();
                return data;
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Gettabledata")]
        public IEnumerable<JDC_data_unique> Gettabledata()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.JDC_data_unique.ToArray();
                return data;
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetJDCColumnName")]
        public List<string> GetJDCColumnName()
        {
            try
            {
                var exceptionList = new List<string> {  "Filepath"
      ,"L"
      ,"a"
      ,"b"
      ,"DL"
      ,"Da"
      ,"Db"
      ,"DE"
      ,"unique_ID"
       ,"TOH"
        ,"sheet_name",
          "ID",
             "importedDate"  };
                var names = typeof(JDC_data_unique).GetProperties().Where(property => !exceptionList.Contains(property.Name))
                         .Select(property => property.Name)
                         .ToList();
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());

                return names;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/JDCExcelData")]
        public string JDCExcelData()
        {
            string message = "";
            string fileName = "JDCExcel.xlsx";
            string path = "Files";
            string test_id = "";
            path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path, fileName);

            //   HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities())
            {
                try
                {

                    if (httpRequest.Files.Count > 0)
                    {
                        HttpPostedFile file = httpRequest.Files[0];
                        Stream stream = file.InputStream;

                        IExcelDataReader reader = null;

                        if (file.FileName.EndsWith(".xls"))
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else if (file.FileName.EndsWith(".xlsx"))
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        else
                        {
                            message = "This file format is not supported";
                        }

                        DataSet excelRecords = reader.AsDataSet();
                        bool condition = CheckJDCExcel(path, excelRecords);

                        reader.Close();
                        if (condition == true)
                        {
                            List<int> countdata = new List<int> { };
                            countdata.Add(2);
                            
                            for (int l = 0; l < excelRecords.Tables.Count; l++)
                            {
                                
                                var finalRecords = excelRecords.Tables[l];
                                test_id = finalRecords.Rows[0][0].ToString();
  
                                for (int i = 0; i < finalRecords.Rows.Count; i++)
                                {
                                    if (finalRecords.Rows[i][0].ToString() == "Sample code")
                                    {
                                        countdata.Add(i);
                                    }
                                }
                                JDC_data_stage JDC = new JDC_data_stage();
                                JDC.filename = finalRecords.Rows[0][0].ToString() + "Test.xlsx";
                                JDC.sheet_name = excelRecords.Tables[l].TableName;
                                JDC.Test_ID = finalRecords.Rows[0][0].ToString();
                                JDC.Test_name = finalRecords.Rows[0][1].ToString();
                                JDC.test_type = finalRecords.Rows[0][2].ToString();
                                JDC.test_location = finalRecords.Rows[2][6].ToString();
                                JDC.Test_Date = DateTime.Parse(finalRecords.Rows[1][3].ToString());
                                JDC.Time = double.Parse(finalRecords.Rows[1][1].ToString());
                                JDC.Time_metric = finalRecords.Rows[1][2].ToString();
                                JDC.TOH = double.Parse(finalRecords.Rows[1][0].ToString());
                                bool RETflag = false;
                                bool DLFlag = false;
                                for (int j = 0; j < countdata.Count; j++)
                                {
                                    try
                                    {
                                        for (int k = countdata[j], q = 3; k < countdata[j + 1];)
                                        {
                                            if (finalRecords.Rows[q][6].ToString() != "")
                                            {
                                                if (finalRecords.Rows[k][0].ToString() != "null" && finalRecords.Rows[k][0].ToString() != "")
                                                {
                                                    if (finalRecords.Rows[k][7].ToString() == "RET")
                                                    {
                                                        RETflag = true;
                                                        DLFlag = false;
                                                        k++;
                                                    }
                                                    else if (finalRecords.Rows[k][6].ToString() == "DL*")
                                                    {
                                                        // RETflag = false;
                                                        DLFlag = true;
                                                        k++;
                                                    }
                                                    if (finalRecords.Rows[k][0].ToString().All(char.IsDigit) == true && finalRecords.Rows[k][0].ToString() != "")
                                                    {
                                                        JDC.Time = double.Parse(finalRecords.Rows[k][1].ToString());
                                                        JDC.Time_metric = finalRecords.Rows[k][2].ToString();
                                                        JDC.TOH = double.Parse(finalRecords.Rows[k][0].ToString());
                                                        JDC.Test_Date = DateTime.Parse(finalRecords.Rows[k][3].ToString());
                                                        k++;
                                                    }

                                                    else if (finalRecords.Rows[k][0].ToString() != "")
                                                    {
                                                        if (finalRecords.Rows[k][6].ToString() != "")
                                                        {
                                                            JDC.Sample_code = finalRecords.Rows[k][0].ToString();

                                                            if (finalRecords.Rows[k][2].ToString() != "")
                                                            {
                                                                JDC.Gloss = double.Parse(finalRecords.Rows[k][2].ToString());
                                                            }
                                                            JDC.RET = finalRecords.Rows[q][7].ToString();
                                                            JDC.HBU = finalRecords.Rows[q][8].ToString() != "" ? double.Parse(finalRecords.Rows[q][8].ToString()) : 0;
                                                            JDC.Color = finalRecords.Rows[q][9].ToString();
                                                            JDC.Formulation = finalRecords.Rows[q][10].ToString();
                                                            JDC.Comments = finalRecords.Rows[q][11].ToString();
                                                            JDC.test_location_id = finalRecords.Rows[q][6].ToString();
                                                            q++;
                                                            JDC.L = finalRecords.Rows[k][3].ToString() != "" ? double.Parse(finalRecords.Rows[k][3].ToString()) : 0;
                                                            JDC.a = finalRecords.Rows[k][4].ToString() != "" ? double.Parse(finalRecords.Rows[k][4].ToString()) : 0;
                                                            JDC.b = finalRecords.Rows[k][5].ToString() != "" ? double.Parse(finalRecords.Rows[k][5].ToString()) : 0;
                                                            JDC.Sample_name = finalRecords.Rows[k][1].ToString();
                                                            if (DLFlag == false)
                                                            {
                                                                JDC.Da = 0;
                                                                JDC.Db = 0;
                                                                JDC.DE = 0;
                                                                JDC.DL = 0;
                                                            }
                                                            else
                                                            {
                                                                JDC.DL = finalRecords.Rows[k][6].ToString() != "" ? double.Parse(finalRecords.Rows[k][6].ToString()) : 0;
                                                                JDC.Da = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][7].ToString()) : 0;
                                                                JDC.Db = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][8].ToString()) : 0;
                                                                JDC.DE = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][9].ToString()) : 0;
                                                            }

                                                            JDC.importedDate = DateTime.Now;
                                                            JDC.isUpdated = false;

                                                            JDC.unique_ID = JDC.test_location_id + "-" + JDC.TOH + "-" + k;
                                                            objEntity.JDC_data_stage.Add(JDC);


                                                            int output1 = objEntity.SaveChanges();

                                                            k++;
                                                        }
                                                        else { k++; }

                                                    }
                                                    else { k++; }
                                                }
                                                else { k++; }
                                            }
                                            else { k++; }
                                            // k++;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        continue;
                                    }
                                }
                                var lastItem = countdata.LastOrDefault();
                                for (int k = lastItem + 1, q = 3; k < finalRecords.Rows.Count; k++)
                                {
                                    if (finalRecords.Rows[q][6].ToString() != "")
                                    {
                                        JDC.Sample_code = finalRecords.Rows[k][0].ToString();

                                        if (finalRecords.Rows[k][2].ToString() != "")
                                        {
                                            JDC.Gloss = double.Parse(finalRecords.Rows[k][2].ToString());
                                        }
                                        JDC.RET = finalRecords.Rows[q][7].ToString();
                                        JDC.HBU = finalRecords.Rows[q][8].ToString() != "" ? double.Parse(finalRecords.Rows[q][8].ToString()) : 0;
                                        JDC.Color = finalRecords.Rows[q][9].ToString();
                                        JDC.Formulation = finalRecords.Rows[q][10].ToString();
                                        JDC.Comments = finalRecords.Rows[q][11].ToString();
                                        JDC.test_location_id = finalRecords.Rows[q][6].ToString();
                                        q++;
                                        JDC.Sample_name = finalRecords.Rows[k][1].ToString();
                                        JDC.L = finalRecords.Rows[k][3].ToString() != "" ? double.Parse(finalRecords.Rows[k][3].ToString()) : 0;
                                        JDC.a = finalRecords.Rows[k][4].ToString() != "" ? double.Parse(finalRecords.Rows[k][4].ToString()) : 0;
                                        JDC.b = finalRecords.Rows[k][5].ToString() != "" ? double.Parse(finalRecords.Rows[k][5].ToString()) : 0;

                                        JDC.DL = finalRecords.Rows[k][6].ToString() != "" ? double.Parse(finalRecords.Rows[k][6].ToString()) : 0;
                                        JDC.Da = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][7].ToString()) : 0;
                                        JDC.Db = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][8].ToString()) : 0;
                                        JDC.DE = finalRecords.Rows[k][7].ToString() != "" ? double.Parse(finalRecords.Rows[k][9].ToString()) : 0;
                                        // JDC.test_location_id = finalRecords.Rows[0][0].ToString() + "-" + finalRecords.Rows[k][0].ToString() + "QUV";
                                        JDC.importedDate = DateTime.Now;

                                        JDC.unique_ID = JDC.test_location_id + "-" + JDC.TOH + "-" + k;
                                        JDC.isUpdated = false;
                                        objEntity.JDC_data_stage.Add(JDC);


                                        int output1 = objEntity.SaveChanges();
                                    }
                                }
                            }
                            message = "success "+test_id;
                        }
                        else
                        {
                            message = "Please upload valid excel sheet";
                        }
                    }
                }
                catch (Exception e)
                {
                    message = e.Message + e.InnerException;
                }
            }

            return message;
        }

        private bool CheckJDCExcel(string path, DataSet excelRecords)
        {
            bool condition = true;
            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"; // Extra blank space cannot appear in Office 2007 and the last version. And we need to pay attention on semicolon.  
                                                                                                                                             //   string connstring = Provider = Microsoft.JET.OLEDB .4 .0;
                                                                                                                                             //Data Source = " + path + ";
                                                                                                                                             //    Extended Properties = " 'Excel 8.0;HDR=NO;IMEX=1';"; //This connection string is appropriate for Office 2007 and the older version. We can select the most suitable connection string according to Office version or our program.  
            using (OleDbConnection conn = new OleDbConnection(connstring))
            {
                conn.Open();
                System.Data.DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" }); //Get All Sheets Name  
                string firstSheetName = sheetsName.Rows[0][2].ToString(); //Get the First Sheet Name  
                string query = "select * from  [QUV$]";
                OleDbConnection objConn;
                OleDbDataAdapter oleDA;
                System.Data.DataTable dt = new System.Data.DataTable();
                objConn = new OleDbConnection(connstring);
                objConn.Open();
                oleDA = new OleDbDataAdapter(query, objConn);
                oleDA.Fill(dt);
                objConn.Close();
                oleDA.Dispose();
                objConn.Dispose();
                List<int> countdata = new List<int> { };
                for (int l = 0; l < excelRecords.Tables.Count; l++)
                {
                    var finalRecords = excelRecords.Tables[l];
                    for (int i = 0; i < finalRecords.Rows.Count; i++)
                    {
                        if (finalRecords.Rows[i][0].ToString() == "Sample code")
                        {
                            countdata.Add(i);
                        }
                    }

                    for (int j = 0; j < 12; j++)
                    {
                        if (j != 6)
                        {
                            if (finalRecords.Rows[2][j].ToString() == dt.Rows[2][j].ToString())
                            {
                                condition = true;
                            }
                            else
                            {
                                condition = false;
                                return condition;
                            }
                        }
                    }

                    for (int i = 1; i < countdata.Count; i++)
                    {
                        for (int j = 0; j < 10; j++)
                        {
                            if (j != 6)
                            {
                                if (finalRecords.Rows[countdata[i]][j].ToString() == dt.Rows[10][j].ToString())
                                {
                                    condition = true;
                                }
                                else
                                {
                                    condition = false;
                                    return condition;
                                }
                            }
                        }
                    }
                    countdata = new List<int> { };
                }
            }
            return condition;
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/updateJDCData")]
        public string updateJDCData(object test_id)
        {
            string[] alldata = ((IEnumerable)test_id).Cast<object>()
                          .Select(x => x.ToString()).ToArray();
            var tmp = alldata[0];
            try
            {

                // int a = 0;
                int a = objEntity.updateJDCData(tmp);
                return "Success";

            }
            catch (Exception e)
            {

                string m = e.Message;
                return e.InnerException.ToString();
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/DeleteJDC")]
        public string DeleteJDC(object test_id)
        {
            string[] alldata = ((IEnumerable)test_id).Cast<object>()
                         .Select(x => x.ToString()).ToArray();
            var tmp = alldata[0];
            try
            {
                using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
                {

                    var Test_ID = (from c in db.JDC_data_unique
                                     where c.Test_ID == tmp
                                   select c);
                    db.JDC_data_unique.RemoveRange(Test_ID);
                    db.SaveChanges();
                }

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}