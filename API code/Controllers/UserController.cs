﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web.Http;
using System.Collections;
using System.Text.RegularExpressions;

namespace CTWeather_API.Controllers
{
    public class UserController : AbstractController
    {
        // GET: User
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetUser")]
        public List<object> GetUser()
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.ct_siding_user.Join(objEntity.UserRoles, a => a.SiteAdmin, u => u.id,
                         (a, u) => new
                {
                    SGI = a.SGI,
                    displayName = a.displayName,
                    mail=a.mail,
                    Role=u.Role,
                    Status = a.IsDelete==false ? "DeActivate" : "Activate"
                }).ToList<object>();
                return data;
                //   return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetUserBySGID/{SGID}")]
        public IEnumerable<ct_siding_user> GetUserBySGID(string SGID)
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
                var data = objEntity.ct_siding_user.Where(p=> p.SGI==SGID).ToArray();
                return data;
                //   return data.ToList();
                // return json;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetUserRoleBySGID/{SGID}")]
        public List<object> GetUserRoleBySGID(string SGID)
        {
            try
            {
                //  var json = new JavaScriptSerializer().Serialize(objEntity.JDC_data_unique.ToList());
              //  var data = objEntity.ct_siding_user.Where(p => p.SGI == SGID).ToArray();
              //  return data;

                return objEntity.ct_siding_user
                   .Join(objEntity.UserRoles, a => a.SiteAdmin, u => u.id,
                         (a, u) => new { a, u })
                  
                   .Where(r => r.a.SGI==SGID)
                   .Select(q => new 
                   {
                       Role = q.u.Role,
                     
                   })
                   .ToList<object>();
                //   return data.ToList();
                
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/DeleteUser/{SGID}/{value}")]
        public string DeleteUser(string SGID,string value)
        {

            try
            {
                using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
                {

                    (from c in db.ct_siding_user
                                     where c.SGI == SGID
                                     select c).ToList().ForEach(x => x.IsDelete = bool.Parse(value));
                    
                    db.SaveChanges();
                }

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/InsertUser")]
        public IHttpActionResult InsertUser(dynamic objStudent)
        {
            string[] alldata = ((IEnumerable)objStudent).Cast<object>()
                                 .Select(x => x.ToString())

                                .ToArray();
            string Name = (string)alldata[0];

            int startindex = Name.IndexOf('(');
            int endindex = Name.IndexOf(')');
            string outputstring = Name.Substring(startindex + 1, endindex - startindex - 1);
            ct_siding_user user1 = Repository.FindBy<ct_siding_user>(u => u.SGI == outputstring).FirstOrDefault();
            try
            {
                if (user1 == null)
                {
                    ct_siding_user user = new ct_siding_user();
                    user.displayName = alldata[0];
                    string[] rowdata = alldata[0].Split(null);
                    user.FirstName = rowdata[0];
                    user.LastName = rowdata[1].Trim();
                    user.SiteAdmin = double.Parse(alldata[1]);
                   // var conn = "Data source = LT106247; Database = Prod_SGNA_ADEX_Master; User Id = sa; Password = Sa@1234";
                       var conn = "Data source=H90SV30600825WB; Database=Prod_SGNA_ADEX_Master;User Id=SGCSDevelopmentSQLAccount;Password=!Flick3R!*@C4";
                    using (SqlConnection connection = new SqlConnection(conn)
                        )
                    {
                        // Creating SqlCommand objcet   
                        SqlCommand cm = new SqlCommand("select * from tblMaster_Accounts where SGI='" + outputstring + "' ", connection);
                        // Opening Connection  
                        connection.Open();
                        // Executing the SQL query  
                        SqlDataReader sdr = cm.ExecuteReader();

                        while (sdr.Read())
                        {
                            user.SGI = sdr["SGI"].ToString();
                            user.mail = sdr["EmailAddress"].ToString();

                            // Console.WriteLine(sdr["Name"] + ",  " + sdr["Email"] + ",  " + sdr["Mobile"]);

                        }

                    }
                    objEntity.ct_siding_user.Add(user);

                    objEntity.SaveChanges();


                    return Ok("User Added Successflly");
                }
                else
                {
                    return Ok("User Alredy Exists");
                }
            }
            catch(Exception e)
            {
                var m="";
                m = e.Message;
                return Ok(e.InnerException.ToString());
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/EditUser")]
        public IHttpActionResult EditUser(dynamic objStudent)
        {
            string[] alldata = ((IEnumerable)objStudent).Cast<object>()
                                 .Select(x => x.ToString())

                                .ToArray();
            string Name = (string)alldata[0];
          //  int startindex = Name.IndexOf('(');
          //  int endindex = Name.IndexOf(')');
          //  string outputstring = Name.Substring(startindex + 1, endindex - startindex - 1);
            ct_siding_user user = new ct_siding_user();
           
                string[] rowdata = alldata[0].Split(new string[] { "," }, StringSplitOptions.None);
           // var conn = "Data source = LT106247; Database = Prod_SGNA_ADEX_Master; User Id = sa; Password = Sa@1234";
               var conn = "Data source=H90SV30600825WB; Database=Prod_SGNA_ADEX_Master;User Id=SGCSDevelopmentSQLAccount;Password=!Flick3R!*@C4";

            using (SqlConnection connection = new SqlConnection(conn)
            )
            {
                // Creating SqlCommand objcet   
                SqlCommand cm = new SqlCommand("select * from tblMaster_Accounts where SGI='" + (string)alldata[2] + "' ", connection);
                // Opening Connection  
                connection.Open();
                // Executing the SQL query  
                SqlDataReader sdr = cm.ExecuteReader();

                while (sdr.Read())
                {
                    user.SGI = sdr["SGI"].ToString();
                    user.mail = sdr["EmailAddress"].ToString();

                    // Console.WriteLine(sdr["Name"] + ",  " + sdr["Email"] + ",  " + sdr["Mobile"]);

                }

            }

            using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
            {

                (from c in db.ct_siding_user
                 where c.SGI == user.SGI
                 select c).ToList().ForEach(x => x.SiteAdmin = double.Parse(alldata[1]));

                db.SaveChanges();
            }
          
                return Ok("User Updated Successflly");
           
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetAllUser/{name}")]
        public List<object> GetAllUser(string name)
        {
            try
            {
                var employeemodellist = new List<object>();
               // var conn = "Data source = LT106247; Database = Prod_SGNA_ADEX_Master; User Id = sa; Password = Sa@1234";
              var conn = "Data source=H90SV30600825WB; Database=Prod_SGNA_ADEX_Master;User Id=SGCSDevelopmentSQLAccount;Password=!Flick3R!*@C4";
       
                if (name != "")
                {
                    using (SqlConnection connection = new SqlConnection(conn))
                    {
                        // Creating SqlCommand objcet   
                        SqlCommand cm = new SqlCommand("select FirstName,LastName,SGI from tblMaster_Accounts where FirstName like '%" + name + "%' or LastName like '%" + name + "%' ", connection);
                        // Opening Connection  
                        connection.Open();
                        // Executing the SQL query  
                        SqlDataReader sdr = cm.ExecuteReader();

                        while (sdr.Read())
                        {
                            // Console.WriteLine(sdr["Name"] + ",  " + sdr["Email"] + ",  " + sdr["Mobile"]);
                            employeemodellist.Add(sdr["LastName"] + " " + sdr["FirstName"] + " (" + sdr["SGI"] + ")");
                        }
                        return employeemodellist.ToList<object>();
                    }
                }
                else
                {
                    employeemodellist.Add("No data found");
                    return employeemodellist.ToList<object>();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetAllUser/")]
        public List<object> GetUserAll()
        {
            try
            {
                var employeemodellist = new List<object>();
                // SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                
                   
             //   employeemodellist.Add(sdr["FirstName"] + ", " + sdr["LastName"]);
                return employeemodellist.ToList<object>();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}