﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CTWeather_API.Controllers
{
    public class TestConditionController : AbstractController
    {
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/InsertXenonTestCondition")]
        public string InsertXenonTestCondition(object Testdata)
        {
            string m = "";

            using (SGRNADataScienceEntities db = new SGRNADataScienceEntities())
            {
                db.Xe_test_param.ToList().ForEach(c =>
                {
                    c.IsDelete = true;
                });
                db.SaveChanges();
            }

            string[] alldata = ((IEnumerable)Testdata).Cast<object>()
                        .Select(x => x.ToString())

                       .ToArray();
            String query = "";

            for (int i = 1; i < alldata.Length; i++)
            {

                string val1 = Regex.Replace(alldata[i], @"\s+", "").Replace("[\"\",", "").Replace(",\"\"]", "").Replace("\"",@"'");

                if (val1 != "[]")
                {

                    List<string> names = objEntity.GetTestConditionColumn().ToList();
                    string column = "";

                    for (int j = 5; j < names.Count; j++)
                    {
                        column = column + "," + names[j];
                    }
                    column = column.TrimEnd(',');
                    try
                    {
                        SqlConnection conn = new SqlConnection("Data source = H90SV30600825WB; Database = SGRNADataScience; User Id = SGCSDevelopmentSQLAccount; Password = !Flick3R!*@C4");
                        //   SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                        conn.Open();
                        query = "INSERT INTO dbo.Xe_test_param ([Large_Xenon],[Param],[Small_Xenon]" + column + ") VALUES (" + val1 + ")";

                          SqlCommand command = new SqlCommand(query, conn);


                           command.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (Exception e)
                    {
                        m = e.Message;
                       // return e.InnerException.ToString();
                    }

                }
            }
            return query;
        }



        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/DeleteXenonCondition/{Param}")]
        public string DeleteCondition(string Param)
        {

            try
            {
               // SqlConnection conn = new SqlConnection("Data source = H90SV30600825WB; Database = SGRNADataScience; User Id = SGCSDevelopmentSQLAccount; Password = !Flick3R!*@C4");
                  SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                conn.Open();
                String query = "update [dbo].[Xe_test_param] set IsDelete=1  where Param='" + Param + "'";

                SqlCommand command = new SqlCommand(query, conn);

                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = command;
                ad.Fill(dt);
                command.ExecuteNonQuery();

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/AddColumn/{columnName}")]
        public string AddColumn(string columnName)
        {

            try
            {
                objEntity.addcolumn(columnName);

                return "1";
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetTestConditionColumn")]
        public List<string> GetTestConditionColumn()
        {

            try
            {
                var names = objEntity.GetTestConditionColumn().ToList();
                return names;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetTestCondition")]
        public string GetTestCondition()
        {

            try
            {
                //   var names = objEntity.GetTestCondition().ToList();
                //   return names;
                // SqlConnection conn = new SqlConnection("Data source = H90SV30600825WB; Database = SGRNADataScience; User Id = SGCSDevelopmentSQLAccount; Password = !Flick3R!*@C4");
                SqlConnection conn = new SqlConnection("Data source=LT106247; Database=SGRNADataScience;User Id=sa;Password=Sa@1234");
                conn.Open();
                String query = "SELECT * from [dbo].[Xe_test_param] where IsDelete is null";

                SqlCommand command = new SqlCommand(query, conn);

                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = command;
                ad.Fill(dt);
                command.ExecuteNonQuery();
                conn.Close();
                string JSONresult;
                JSONresult = JsonConvert.SerializeObject(dt);
                return JSONresult;


            }
            catch (Exception)
            {
                throw;
            }
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}