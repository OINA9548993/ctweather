﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using CTWeather_API;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;

namespace CTWeather_API.Controllers
{
    public class DashboardController : AbstractController
    {
        // GET: Dashboard
        SGRNADataScienceEntities objEntity = new SGRNADataScienceEntities();

       

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/sso")]
        public IHttpActionResult Sso(string ticket)
        {
            var token = string.Empty;
            var SGID = string.Empty;
            var Name = string.Empty;
            var URL = string.Empty;
            var siteAdmin = string.Empty;

            if (!string.IsNullOrWhiteSpace(ticket))
            {
                // Retrieve SGId from CAS server
                var sgId = RetrieveSGIdFromTicket(ticket, Request.RequestUri.GetLeftPart(UriPartial.Path)); //"K6698487";
                if (!string.IsNullOrWhiteSpace(sgId))
                {
                    // Check if user exists in database
                    var user = Repository.FindBy<ct_siding_user>(u => u.SGI == sgId && (u.IsDelete == false))

                         .FirstOrDefault();
                    if (user != null)
                    {
                        UpdateToken(ticket, user.ID);
                        token = user.Token1.value;
                        SGID = user.SGI;
                        Name = user.FirstName + " " + user.LastName;
                       
                    URL= "http://localhost:4200/authorize?SGID="+SGID+"&Name="+Name + "&ticket=" + ticket;
                      //  URL = "https://weather.certainteed.com/weather/authorize?SGID=" + SGID + "&Name=" + Name + "&ticket=" + ticket;
                       // URL = "https://digital-team-825wb.certainteed.com/weather/authorize?SGID=" + SGID + "&Name=" + Name;
                    }
                    else
                    {
                      // URL = "http://localhost:4200/Error";
                     //   URL = "http://digital-team-825wb.certainteed.com/weather/Error";
                        URL = "https://weather.certainteed.com/weather/Error";
                    }

                  
                   // return user;
                }
            }

            return Redirect(URL);
         //  return Redirect("https://digital-team-805wb.certainteed.com/weather/authorize?SGID=" + SGID + "&Name=" + Name);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Logout")]
        public IHttpActionResult Logout()
        {
            //HttpSessionStateBase.Clear();
            //HttpSessionStateBase.Abandon();
            //Response.Cookies.Clear();
            //CasAuthentication.ClearAuthCookie();
            var URL = string.Empty;
         //  URL = "https://digital-team-825wb.certainteed.com/weather/Logout";

            URL = "https://weather.certainteed.com/weather/Logout";
            return Redirect(URL);
            //  return Redirect("https://digital-team-805wb.certainteed.com/weather/authorize?SGID=" + SGID + "&Name=" + Name);
        }

        private void UpdateToken(string token,int UserID)
        {
            objEntity.updatetoken(token,UserID);
        }

        private string RetrieveSGIdFromTicket(string ticket, string service)
        {
            using (var webClient = new WebClient())
            {
               //  var validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
               var validateUrl = "https://websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
                var stream = webClient.OpenRead(validateUrl);
                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    var response = reader.ReadToEnd();
                    var ssoResult = response.Split(new[] { '\n' }, StringSplitOptions.None);
                    if (ssoResult[0].Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return ssoResult[1];
                    }
                }
            }
            return null;
        }

     

    }

    public class TestParam
    {
        public string Param { get; set; }
        public string largeXenon { get; set;}
        public string smallXenon { get; set; }
    } 
}