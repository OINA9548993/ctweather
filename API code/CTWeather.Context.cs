﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CTWeather_API
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SGRNADataScienceEntities : DbContext
    {
        public SGRNADataScienceEntities()
            : base("name=SGRNADataScienceEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ct_siding_user> ct_siding_user { get; set; }
        public virtual DbSet<JDC_data_stage> JDC_data_stage { get; set; }
        public virtual DbSet<JDC_data_unique> JDC_data_unique { get; set; }
        public virtual DbSet<pp_data_stage> pp_data_stage { get; set; }
        public virtual DbSet<pp_data_unique> pp_data_unique { get; set; }
        public virtual DbSet<QUV_test_param> QUV_test_param { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<JDC_data_stage_bk> JDC_data_stage_bk { get; set; }
        public virtual DbSet<pp_data_stage_bk> pp_data_stage_bk { get; set; }
        public virtual DbSet<pp_data_unique_bk> pp_data_unique_bk { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Xe_test_param> Xe_test_param { get; set; }
        public virtual DbSet<Xe_test_param_bk> Xe_test_param_bk { get; set; }
    
        public virtual int ImportfromExcel()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ImportfromExcel");
        }
    
        public virtual int DeleteTables()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteTables");
        }
    
        public virtual int updateData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updateData");
        }
    
        public virtual int updateJDCData(string test_id)
        {
            var test_idParameter = test_id != null ?
                new ObjectParameter("test_id", test_id) :
                new ObjectParameter("test_id", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updateJDCData", test_idParameter);
        }
    
        public virtual int updatetoken(string token, Nullable<int> userid)
        {
            var tokenParameter = token != null ?
                new ObjectParameter("token", token) :
                new ObjectParameter("token", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updatetoken", tokenParameter, useridParameter);
        }
    
        public virtual ObjectResult<GetJDCData_Result> GetJDCData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetJDCData_Result>("GetJDCData");
        }
    
        public virtual int addcolumn(string columnName)
        {
            var columnNameParameter = columnName != null ?
                new ObjectParameter("columnName", columnName) :
                new ObjectParameter("columnName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("addcolumn", columnNameParameter);
        }
    
        public virtual ObjectResult<string> GetTestConditionColumn()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetTestConditionColumn");
        }
    
        public virtual ObjectResult<GetTestCondition_Result> GetTestCondition()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTestCondition_Result>("GetTestCondition");
        }
    
        public virtual ObjectResult<GetMICData_Result> GetMICData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetMICData_Result>("GetMICData");
        }
    }
}
