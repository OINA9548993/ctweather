﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CTWeather_API
{
    public interface IRepository
    {
        SGRNADataScienceEntities Context { get; }

        IQueryable<T> GetAll<T>() where T : class;

        IQueryable<T> FindBy<T>(Expression<Func<T, bool>> predicate) where T : class;

        T GetById<T>(int id) where T : class;

        void Add<T>(T entity) where T : class;

        void Update<T>(T entity) where T : class;

        void Delete<T>(T entity) where T : class;

        bool Save();
    }
}
