﻿using CTWeather_API;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CTWeather_API
{
   public class Repository:IRepository
    {
        private readonly SGRNADataScienceEntities context;
        public SGRNADataScienceEntities Context => context;

        public Repository(SGRNADataScienceEntities context)
        {
            this.context = context;
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return context.Set<T>();
        }

        public IQueryable<T> FindBy<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return context.Set<T>().Where(predicate);
        }

        public T GetById<T>(int id) where T : class
        {
            return context.Set<T>().Find(id);
        }

        public void Add<T>(T entity) where T : class
        {

            context.Entry(entity).State = EntityState.Added;
        }

        public void Update<T>(T entity) where T : class
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete<T>(T entity) where T : class
        {
            context.Entry(entity).State = EntityState.Deleted;
        }

        public bool Save()
        {
            return context.SaveChanges() > 0;
        }
    }
}
