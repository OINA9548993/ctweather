//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CTWeather_API
{
    using System;
    using System.Collections.Generic;
    
    public partial class Token
    {
        public string value { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public int UserId { get; set; }
    
        public virtual ct_siding_user ct_siding_user { get; set; }
    }
}
